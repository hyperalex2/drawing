﻿Public Class LogHandle
    ' Want to hold a log of messages, times, and then save them easily.
    Private _AllLogs As New List(Of String)
    Private _CommandLog As New List(Of String)
    Private _DrawLog As New List(Of String)
    Private _ChatLog As New List(Of String)
    Private _Errors As New List(Of String)
    Private _Immediate As New List(Of String)
    Private StartTime As DateTime
    Private EndTime As DateTime
    Private FolderPath As String
    Public LogInUse As Boolean = False
    Public Sub WaitForLogUse()
        While LogInUse = True
            Threading.Thread.Sleep(1)
        End While
        LogInUse = True
    End Sub

    ''' <summary>
    ''' All messages are logged here
    ''' </summary>
    Public ReadOnly Property AllLog As List(Of String)
        Get
            Dim _log As List(Of String) = New List(Of String)
            WaitForLogUse()
            For Each line As String In _AllLogs
                If _AllLogs.IndexOf(line) > 8 Then
                    _log.Add(line)
                End If
            Next
            LogInUse = False
            Return _log
        End Get
    End Property
    ''' <summary>
    ''' Logs related to admin messages, commands etc.
    ''' </summary>
    Public ReadOnly Property CommandLog As List(Of String)
        Get
            Dim _log As List(Of String) = New List(Of String)
            WaitForLogUse()
            For Each line As String In _CommandLog
                _log.Add(line)
            Next
            LogInUse = False
            Return _log
        End Get
    End Property
    ''' <summary>
    ''' Lobby and admin chat
    ''' </summary>
    Public ReadOnly Property ChatLog As List(Of String)
        Get
            Dim _log As List(Of String) = New List(Of String)
            WaitForLogUse()
            For Each line As String In _ChatLog
                _log.Add(line)
            Next
            LogInUse = False
            Return _log
        End Get
    End Property
    Public ReadOnly Property SavePath As String
    Public ReadOnly Property ReadLog As String
        Get
            Dim _log As String = ""
            WaitForLogUse()
            For Each line As String In _AllLogs
                _log += line
            Next
            LogInUse = False
            Return _log
        End Get
    End Property

    ''' <summary>
    ''' Log for all incoming messages to the server
    ''' Does not log to the all.txt log too
    ''' </summary>
    Public Sub LogImmediate(msg As String, Optional overrideTime As Boolean = False)
        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _Immediate.Add(msg)
        LogInUse = False
    End Sub

    ''' <summary>
    ''' Logs an error, to specific log and all
    ''' </summary>
    Public Sub LogError(msg As String, Optional overrideTime As Boolean = False)
        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _AllLogs.Add(msg)
        _Errors.Add(msg)
        LogInUse = False
    End Sub

    ''' <summary>
    ''' Logs a chat message, to chatlog and all
    ''' </summary>
    Public Sub LogChat(msg As String, Optional overrideTime As Boolean = False)
        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _AllLogs.Add(msg)
        _ChatLog.Add(msg)
        LogInUse = False
    End Sub

    Public Sub LogCommand(msg As String, Optional overrideTime As Boolean = False)
        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _AllLogs.Add(msg)
        _CommandLog.Add(msg)
        LogInUse = False
    End Sub

    Public Sub LogDraw(msg As String, Optional overrideTime As Boolean = False)

        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _AllLogs.Add(msg)
        _DrawLog.Add(msg)
        LogInUse = False
    End Sub

    Public Sub LogMisc(msg As String, Optional overrideTime As Boolean = False)

        If msg.Contains(",") AndAlso msg.Contains("-") AndAlso msg.Contains(".") Then
            Return
        End If
        WaitForLogUse()
        If overrideTime = False Then
            msg = DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg
        End If
        _AllLogs.Add(msg)
        LogInUse = False
    End Sub

    Public Sub SaveLog(reason As String)
        EndTime = DateTime.Now()
        WaitForLogUse()
        If reason.ToLower().Contains("close") Then
            _AllLogs.Add("-- Closed at " + EndTime.ToString("dd-MM-yyyy hh:mm:ss.fff") + ", reason: " + reason)
        Else
            _AllLogs.Add("-- Saved at " + EndTime.ToString("dd-MM-yyyy hh:mm:ss.fff") + ", reason: " + reason)
        End If
        If IO.Directory.Exists(FolderPath) = False Then IO.Directory.CreateDirectory(FolderPath)
        System.IO.File.WriteAllLines(FolderPath + "all.txt", _AllLogs.ToArray())
        System.IO.File.WriteAllLines(FolderPath + "command.txt", _CommandLog.ToArray())
        System.IO.File.WriteAllLines(FolderPath + "draw.txt", _DrawLog.ToArray())
        System.IO.File.WriteAllLines(FolderPath + "chat.txt", _ChatLog.ToArray())
        System.IO.File.WriteAllLines(FolderPath + "inbound.txt", _Immediate.ToArray())
        LogInUse = False
    End Sub

    Public Sub New(_path As String, versionNumber As Version)
        StartTime = DateTime.Now()
        FolderPath = _path + "log_" + StartTime.ToString().Replace(":", "-").Replace("/", "_") + "\"
        SavePath = FolderPath + "all.txt"
        _AllLogs = New List(Of String)
        _AllLogs.Add("-- Log Init --")
        _AllLogs.Add("Drawing server log file")
        _AllLogs.Add("Client Version: " + versionNumber.ToString + " (" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion.ToString + ")")
        _AllLogs.Add("User OS: " & My.Computer.Info.OSFullName)
        _AllLogs.Add("User OS Version: " & My.Computer.Info.OSVersion)
        _AllLogs.Add("User OS Platform: " & My.Computer.Info.OSPlatform)
        _AllLogs.Add("User Name: " + My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator).ToString)
        _AllLogs.Add("-- Log Continues.. --")
    End Sub
End Class

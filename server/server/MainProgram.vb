﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Windows.Forms
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Module MainProgram
    ' ---- UPDATED BY ALEX: -----
    ' See the Drawing Trello for a full view on changes/things.
    '% indicates the start of a new msg
    'the coords are sent in the form x,y-x,y forming two points (point-point) to draw a line
    '$$$ is.. a separator too? - yes, it is used to denote the start and end of the username (should prevent a coord from getting into a name somehow)
    Dim serverSocket As New TcpListener(getIPaddress(), 8888)
    Dim Users As New Dictionary(Of String, User)
    Dim rand As Random = New Random()
    Dim drawLog As String = "DRAWLOG:"
    Dim votesToStart As Integer = 0
    Dim Painters As New List(Of String)
    Dim isGamePlaying As Boolean = False
    Dim autoStart As Boolean = False
    Dim painter As String
    Public WhoCanVoteToStart As String = "Everyone"
    Public InLobby As Boolean = True

    Public Const SERVER_NAME As String = "Alex's Test Server"
    Public Const MASTERLIST_IP As String = "masterlist.ddns.net"
    Public Const MASTERLIST_PORT As Int32 = 8889
    Public Const RUN_EXTERNALLY As Boolean = False ' this should be true if the server is running in a port-forward state.
    ' it means that the masterlist will show the server's ip as its external one, rather than its internal

    Private ExternalIP As String = ""
    Public ReadOnly Property SERVER_IP As String
        Get
            If RUN_EXTERNALLY Then
                If ExternalIP = "" Then
                    Using wClient As New WebClient()
                        ExternalIP = wClient.DownloadString("http://icanhazip.com/").Replace(vbLf, String.Empty)
                    End Using
                End If
                Return ExternalIP
            End If
            Return getIPaddress().ToString()
        End Get
    End Property


    Public Enum Rank
        Player = 0
        Moderator = 1
        Admin = 2
        Manager = 3
    End Enum
    Public Ignore_Client_Checksum = False ' do we ignore checksums all together?
    Public Latest_Client_Checksum = "" ' latest checksum of Bitbucket client version
    Public AllowChecksums As New List(Of String) ' versions explicitly said as allowed
    Private LastJoinChecksum As String = "" ' set when a client fails to join due to outdated client

    Public _CONFIG As JObject ' config.json file
    Public _ErrorConfig As JObject ' shrug

    Dim BannedUsers As New List(Of Ban)
    Public Class Ban
        Public Name As String
        Public IP As String
        Public ActualName As String
        Public Serial As String
        Public Reason As String
        Public IP_Ban As Boolean
        Public Serial_Ban As Boolean
        Public Name_Ban As Boolean
        Public Sub New(_user As User, type As String, _reason As String)
            Name = _user.Name
            ActualName = _user.ActualName
            Reason = _reason
            IP = _user.IP.ToString()
            Serial = _user.Serial
            Name_Ban = True
            IP_Ban = False
            Serial_Ban = False
            If type = "IP" Then
                IP_Ban = True
            ElseIf type = "Serial" Then
                Serial_Ban = True
            ElseIf type = "Both" Then
                IP_Ban = True
                Serial_Ban = True
            Else
            End If
        End Sub
        Public Sub New()

        End Sub
        Public Overrides Function ToString() As String
            Return Name + "," + IP.ToString() + "," + Serial + "," + IP_Ban.ToString() + "," + Serial_Ban.ToString() + "," + ActualName.Replace(" ", "_")
        End Function
        Public Shared Function FromString(_line As String) As Ban
            Dim lineSplit = _line.Split(",")
            Dim newBan As Ban = New Ban()
            newBan.Name = lineSplit(0)
            newBan.IP = lineSplit(1)
            newBan.Serial = lineSplit(2)
            newBan.IP_Ban = Boolean.Parse(lineSplit(3))
            newBan.Serial_Ban = Boolean.Parse(lineSplit(4))
            newBan.ActualName = lineSplit(5).Replace("_", " ")
            Return newBan
        End Function
    End Class

    Public Difficulty As String = "Easy"
    Public CurrentGame As DrawGame

    Public UsersInUse As Boolean = False
    Public Sub WaitForUsers()
        While UsersInUse
            Threading.Thread.Sleep(1)
        End While
        UsersInUse = True
    End Sub

    Dim LatestVersion As Version = New Version("0.0.0.0")
    Dim ThisThingVersion As Version = New Version("0.0.0.0")
    Public Log As LogHandle
    Public HasRecievedPoint As Boolean = False ' Records whether we have recieved points since last CLEAR message.
    ' possibly should remove this - could be thing causing inability to change colour between CLEAR -> next draw


    Function getIPaddress() As Net.IPAddress
        Dim ipaddress As Net.IPAddress
        Dim strhostname As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = Net.Dns.GetHostEntry(strhostname)

        For Each ipheal As Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ipaddress = ipheal
                Exit For
            End If
        Next
        Return ipaddress
    End Function

    Public Function SendMessageToIP(IP As String, Port As Integer, msg As String) As String
        ' Used for masterlist communication
        Dim recieved As String = ""
        Try
            Using client As New TcpClient(IP, Port)
                Dim data As [Byte]() = System.Text.Encoding.ASCII.GetBytes(msg)
                Dim bytesFrom(client.ReceiveBufferSize) As Byte
                Using stream As NetworkStream = client.GetStream()
                    stream.Write(data, 0, data.Length)
                    stream.Flush()
                    stream.Read(bytesFrom, 0, CInt(client.ReceiveBufferSize))
                End Using ' Masterlist updates to come (wow such excitement)
                recieved = System.Text.Encoding.UTF8.GetString(bytesFrom)
                recieved = recieved.Replace(vbNullChar, String.Empty)
            End Using
        Catch ex As SocketException
            If IP = MASTERLIST_IP Then
                writeToConsole("Unable to connect to masterlist: " + ex.Message)
            End If
        Catch ex As Exception
            writeToConsole("Unable to connect to " + IP + ": " + ex.Message)
        End Try
        Return recieved
    End Function

    Public Sub SendMessageForce(clientSocket As TcpClient, msg As String)
        ' sends message to client
        If String.IsNullOrWhiteSpace(msg) Then Return
        If Not msg.Substring(0, 1) = "%" Then msg = "%" + msg
        Dim broadcastStream As NetworkStream = clientSocket.GetStream()
        broadcastStream.Flush()
        Dim broadcastBytes As [Byte]()
        broadcastBytes = Encoding.UTF8.GetBytes(msg + "`")
        broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length)
        broadcastStream.Flush()
    End Sub

    Public Function IsAlreadyConnected(name As String) As Boolean
        ' prevents a duplicate person from joining
        ' fun times from my chat client when people could impersonate others..
        ' and it would allow them to login due to technically being someone else, lol.
        Try
            WaitForUsers()
            For Each nm As String In Users.Keys
                If name = nm Then Return True
            Next
            Return False
        Catch ex As Exception
        Finally
            UsersInUse = False
        End Try
    End Function

    Public Function IsUserBanned(name As String, ip As IPAddress, serial As String) As Boolean
        ' checks via name, ip and serial, if the ban has those set up.
        For Each item As Ban In BannedUsers
            If item.Name = name Then
                Return True
            End If
            If item.IP_Ban AndAlso ip.ToString() = item.IP.ToString() Then
                Return True
            End If
            If item.Serial_Ban AndAlso serial = item.Serial Then
                Return True
            End If
        Next
        Return False
    End Function


    Private FirstAdminSet As Boolean = False
    Public Function IsAlphaNumeric(ByVal strToCheck As String) As Boolean
        ' maybe it doesnt allowed space?
        Dim pattern As Regex = New Regex("[^a-zA-Z0-9\x20]") ' Allows space as well
        Dim isAlpha As Boolean = Not pattern.IsMatch(strToCheck)
        If isAlpha = False Then Return False
        For Each char_ In strToCheck
            If Not "qwertyuiopasdfghjklzxcvbnm1234567890-_".Contains(char_.ToString().ToLower()) Then
                Return False
            End If
        Next
        Return True
    End Function

#Region "SECTION: User Scores, Saving and Loading"
    Dim SavedUsers As New Dictionary(Of String, UserSave) ' Username - Info
    Private SavedUsersInUse As Boolean = False
    Const SAVE_PATH As String = "Saves\"
    Private Sub WaitForSavedUsers()
        While SavedUsersInUse
            Threading.Thread.Sleep(1)
        End While
        SavedUsersInUse = True
    End Sub
    Public Sub LoadUserInfo()
        Dim savePath As String = SAVE_PATH + "save.info"
        Dim contents As String = ""
        Try
            contents = File.ReadAllText(savePath)
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        If String.IsNullOrWhiteSpace(contents) Then Return
        Dim save As SaveData = JsonConvert.DeserializeObject(Of SaveData)(contents)
        For Each item As UserSave In save.Users
            SavedUsers.Add(item.AccountName, item)
        Next
        BannedUsers = save.Bans
    End Sub
    Public Sub SaveAllUserInfo()
        Dim savePath As String = SAVE_PATH + "save.info"
        writeToConsole("Saving scores of all users")
        WaitForUsers()
        For Each usr As User In Users.Values
            Dim saveNew As New UserSave()
            saveNew.AccountName = usr.UnChangedAccountName
            saveNew.Rank = usr.Rank
            saveNew.Score = usr.Score
            saveNew.Serial = usr.Serial
            If SavedUsers.Keys.Contains(usr.Name) Then
                SavedUsers(usr.Name) = saveNew
            Else
                SavedUsers.Add(usr.Name, saveNew)
            End If
        Next
        UsersInUse = False
        Dim newSave As New SaveData()
        WaitForSavedUsers()
        newSave.Users = SavedUsers.Values.ToList()
        SavedUsersInUse = False
        newSave.Bans = BannedUsers
        Dim str As String = JsonConvert.SerializeObject(newSave)
        Try
            File.WriteAllText(savePath, str)
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
    End Sub

    Public Function GetUserSaveFor(name As String, serial As String) As UserSave
        Try
            WaitForSavedUsers()
            For Each usr As UserSave In SavedUsers.Values
                If usr.AccountName = name AndAlso usr.Serial = serial Then
                    Return usr
                End If
            Next
            Return New UserSave
        Catch ex As Exception
        Finally
            SavedUsersInUse = False
        End Try
    End Function

    Public Structure UserSave
        Dim AccountName As String
        Dim Serial As String
        Dim Score As Integer
        Dim Rank As Integer

        Public Overrides Function ToString() As String
            Return AccountName + " " + Serial + " " + Score.ToString() + " " + Rank.ToString()
        End Function
    End Structure
#End Region

#Region "SECTION:Client usernames to real names"
    Private NameDict As New Dictionary(Of String, String)
    Private nameDictInUse As Boolean = False
    Public Sub HandleStartNames()
        Dim fullNames As New List(Of String) From {"Abdul Shafiq",
            "Alex Chester",
            "Amir Manafikhi",
            "Anushan Mukunthan",
            "Benjamin Pavitt",
            "Borhan Baksh",
            "Charlie Seddon",
            "Curtis Loakes",
            "Danesh Balasubramaniam",
            "Finley Vigor",
            "George Leftwich",
            "Harry Hyde",
            "Harry Wiltshire",
            "Iian Fletcher",
            "Jake Millen",
            "Jared Seymour",
            "Jon Blount",
            "Joshua Edge",
            "Liliana Odjo",
            "Reece Brown",
            "Ryan Griffiths",
            "Ryan Benson",
            "Sohail Mohammed-Khail",
            "Thomas Burns"}
        For Each name As String In fullNames
            Dim allLower As String = name.ToLower()
            Dim firstName As String = allLower.Split(" ")(0)
            Dim lastName As String = allLower.Split(" ")(allLower.Split(" ").Length - 1)
            Dim usrName As String = lastName.Substring(0, 3) + firstName.Substring(0, 3) + "14"
            NameDict(usrName) = name
        Next
        NameDict("thodanst") = "Mr Thomson"
        NameDict("teacher") = "Staff Teacher"
    End Sub
    Public Function GetFullName(usrName As String) As String
        While nameDictInUse
            Threading.Thread.Sleep(1)
        End While
        nameDictInUse = True
        Dim toReturn As String = ""
        For Each nm As String In NameDict.Keys
            If nm = usrName Then toReturn = NameDict(nm)
        Next
        nameDictInUse = False
        If toReturn = "" Then Return usrName
        Return toReturn
    End Function
#End Region

    Public Sub BroadCast_AllClients()
        ' Tells all the clients exactly who is online
        Dim allClients As String = ""
        WaitForUsers()
        For Each usr As User In Users.Values
            allClients = allClients & "." & usr.Name & ";" & usr.Score & ";" & usr.Rank & ";" & usr.ActualName.Replace(" ", "_")
        Next
        UsersInUse = False
        broadcast("%" & allClients.Substring(1) & "ALLCLIENTS") '..while the new client needs the full list of clients connected
    End Sub

    Public Class LocationJson
        ' What gets returned from a whois report - helpful, i guess
        Public IP As String
        Public HostName As String
        Public Loc As String
        Public Org As String
        Public City As String
        Public Region As String
        Public Country As String
        Public Phone As Integer
    End Class

    Private Sub GetLocationOf(ip As IPAddress, usr As String, _thread As Threading.Thread)
        ' Contacts a website that returns the location and other info of a given IP
        ' It SHOULD be "Internal" or "Coventry" andything else and *someone* has joined..
        Dim LOCATION As String = "Unknown"
        Dim ipStr As String = ip.ToString()
        Dim isInternal As Boolean = False
        If ipStr > "192.168.0.0" AndAlso ipStr <= "192.168.255.255" Then
            isInternal = True
        ElseIf ipStr > "10.0.0.0" AndAlso ipStr <= "10.255.255.255" Then
            isInternal = True
        ElseIf ipStr >= "172.16.0.0" AndAlso ipStr <= "172.31.255.255" Then
            isInternal = True
        End If
        If isInternal Then
            LOCATION = "Internal"
        Else
            Try
                Dim myReq As HttpWebRequest = WebRequest.Create("http://ipinfo.io/" & ip.ToString())
                Dim response As HttpWebResponse = myReq.GetResponse()
                Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
                Dim strResponse As String = resStream.ReadToEnd().ToString()
                Dim responses As New List(Of String)
                For Each line As String In strResponse.Split(vbLf)
                    If line <> "{" And line <> "}" Then
                        responses.Add(line.Replace("""", String.Empty).Replace(",", String.Empty).Trim())
                    End If
                Next
                For Each item As String In responses
                    If item.Contains("city") Then
                        LOCATION = item.Replace("city: ", String.Empty)
                        Exit For
                    End If
                Next
            Catch ex As Exception
                Log.LogError(ex.ToString())
                writeToConsole(ex.ToString())
            End Try
        End If
        WaitForUsers()
        For Each _usr As User In Users.Values
            If _usr.Name = usr Then
                _usr.Location = LOCATION
                Exit For
            End If
        Next
        UsersInUse = False
        writeToConsole(usr + "'s location is: " & LOCATION)
        '_thread.Abort()
    End Sub

    Private Function IsValidChecksum(checksum As String) As Boolean
        ' Checksums are eessentially an SHA256 hash of the entire client.exe
        ' If checking them is enabled then:
        ' - The latest bitbucket version is by default added as allowed
        ' - The server can remove, clear, add, save or load a list of allowed client hashes
        ' If a client attempts to join with a different hash than the ones allowed, it is blocked
        ' This means only the latest version (or versions allowed) will be permitted to join
        ' It would also prevent people from changing their clients, as the hash would be different per client
        ' Even one charactor different = entire different hash
        If Ignore_Client_Checksum Then Return True
        For Each sum As String In AllowChecksums
            If sum = checksum Then Return True
        Next
        Return False
    End Function

    Public Sub NewClientHandler()
        Dim clientSocket As TcpClient
        writeToConsole("Starting client handler")
        Dim autoRole As JToken = _CONFIG("auto-role")
        Dim AutoModUsers As New List(Of String) ' users to be given moderator
        Dim AutoAdminUsers As New List(Of String) ' users to be given admin
        Dim AutoManagerUsers As New List(Of String) ' users to be given manager
        For Each type As String In New List(Of String) From {"developers", "admins", "managers"}
            Dim place As JToken = autoRole(type)
            Dim vals As New List(Of String)
            For Each tok As JValue In place.Values
                vals.Add(tok.Value)
            Next
            If type = "developers" Then
                AutoModUsers.AddRange(vals)
            ElseIf type = "admins" Then
                AutoAdminUsers.AddRange(vals)
            Else
                AutoManagerUsers.AddRange(vals)
            End If
        Next
        If AutoModUsers.Count > 0 Then
            writeToConsole("Users will be auto-modded: " & String.Join(", ", AutoModUsers))
        End If
        If AutoAdminUsers.Count > 0 Then
            writeToConsole("Users will be auto-admin: " & String.Join(", ", AutoAdminUsers))
        End If
        If AutoManagerUsers.Count > 0 Then
            writeToConsole("Users will be auto-manager: " & String.Join(", ", AutoManagerUsers))
        End If

        While (True) 'the following listens for new clients
            clientSocket = serverSocket.AcceptTcpClient()
            Dim bytesFrom(65535) As Byte
            Dim dataFromClient As String
            Dim networkStream As NetworkStream = clientSocket.GetStream()
            networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
            dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom)
            Dim ipend As Net.IPEndPoint = clientSocket.Client.RemoteEndPoint
            If String.IsNullOrWhiteSpace(dataFromClient) Then
                writeToConsole("Invalid connection made by IP " & ipend.Address.ToString())
                clientSocket.Close()
                Continue While
            End If
            Dim uName As String = "Error."
            Try
                uName = dataFromClient.Substring(dataFromClient.IndexOf("$$$"), dataFromClient.IndexOf("`")) 'shouldnt it be the index of the second $$$ ?
            Catch ex As ArgumentOutOfRangeException
                writeToConsole("Invalid conneciton made by IP " & ipend.Address.ToString())
                clientSocket.Close()
                Continue While
            End Try
            Dim dataSplit As String() = uName.Split("$$$")
            Dim toBeSerial As String = "None"
            Dim toBeComputerUserName As String = "None"
            Dim clientChecksum As String = "None"
            uName = dataSplit(3)
            If uName.Split(";").Length = 4 Then
                toBeSerial = uName.Split(";")(1)
                toBeComputerUserName = uName.Split(";")(2)
                clientChecksum = uName.Split(";")(3)
                uName = uName.Split(";")(0)
            Else
                Try
                    SendMessageForce(clientSocket, "&CLOSE&An error occured - your connection string was invalid.")
                Catch ex As Exception
                End Try
                clientSocket.Close()
                Continue While
            End If
            If toBeComputerUserName = "None" Then toBeComputerUserName = uName
            If toBeSerial = "None" Then
                writeToConsole("User " & ipend.Address.ToString() & ") has an invalid name/serial")
                SendMessageForce(clientSocket, "&CLOSE&Your client is corrupt, or your name is invalid. Please check your name is alphanumeric only")
                clientSocket.Close()
                Continue While
            End If
            Dim tobeActName As String = GetFullName(toBeComputerUserName)
            Dim setRank As Integer = 0
            If uName.Contains("&Rank.Admin&") Then
                ' If user is on same computer and requests, then they can be made managed.
                ' Note: someone could attempt to spoof their IP? not sure if that would work
                uName = uName.Replace("&Rank.Admin&", String.Empty)
                If ipend.Address.ToString() = getIPaddress.ToString() Then
                    setRank = 3 ' User on same computer
                    writeToConsole("User: " + uName + " made Manager, same computer as Server.")
                End If
            End If
            ' End If
            If IsUserBanned(uName, ipend.Address, toBeSerial) Then
                Dim b As Ban = GetBan(uName)
                writeToConsole("User " & tobeActName & " attempted to join while banned.")
                SendMessageForce(clientSocket, _ErrorConfig.GetValueAt("conn_errors:banned") + vbCrLf + "Reason: " + b.Reason)
                clientSocket.Close()
                Continue While
            End If
            If Not IsAlphaNumeric(uName) Then
                writeToConsole("User " & tobeActName & "(" & ipend.Address.ToString() & ") has an invalid name")
                SendMessageForce(clientSocket, _ErrorConfig.GetValueAt("conn_errors:name"))
                clientSocket.Close()
                Continue While
            End If
            If IsAlreadyConnected(uName) Then
                writeToConsole("User with IP " & ipend.Address.ToString & " attempted same-name join (" & tobeActName & ")")
                SendMessageForce(clientSocket, _ErrorConfig.GetValueAt("conn_errors:alreadyconn"))
                clientSocket.Close()
                Continue While
            End If
            If uName.Contains("Server") Or uName.Contains("Admin") Or uName.Contains("Manager") Then
                writeToConsole("User " & tobeActName & "(" & ipend.Address.ToString() & ") attempted to impersonate")
                SendMessageForce(clientSocket, _ErrorConfig.GetValueAt("conn_errors:impers"))
                clientSocket.Close()
                Continue While
            End If
            If Not IsValidChecksum(clientChecksum) Then
                Dim message As String = ""
                If Latest_Client_Checksum = clientChecksum Then
                    message = _ErrorConfig.GetValueAt("conn_errors:checksum:hasLatest")
                Else
                    message = _ErrorConfig.GetValueAt("conn_errors:checksum:main")
                End If
                writeToConsole("WARN: User has invalid checksum, " & uName & " / " & tobeActName & ": " & clientChecksum)
                LastJoinChecksum = clientChecksum
                Try
                    Dim th = New Thread(Sub() SetClipboard(clientChecksum))
                    th.SetApartmentState(ApartmentState.STA)
                    th.IsBackground = True
                    th.Start()
                    While th.IsAlive
                        th.Join(50)
                    End While
                Catch ex As Exception
                    MsgBox(ex.ToString())
                End Try
                SendMessageForce(clientSocket, message)
                clientSocket.Close()
                Continue While
            End If
            If isGamePlaying Then ' Will display other errors first, so they can get ready
                writeToConsole(tobeActName & " tried to connect mid game")
                SendMessageForce(clientSocket, _ErrorConfig.GetValueAt("conn_errors:inprog"))
                clientSocket.Close()
                Continue While
            End If
            Dim newUser As New User(uName, ipend.Address, setRank, clientSocket, toBeSerial)
            newUser.ActualName = tobeActName
            newUser.UnChangedAccountName = toBeComputerUserName
            Dim usrSave As UserSave = GetUserSaveFor(newUser.UnChangedAccountName, newUser.Serial)
            If Not usrSave.AccountName Is Nothing Then
                newUser.Score = usrSave.Score
                If setRank = 0 Then newUser.Rank = usrSave.Rank
            End If
            If AutoManagerUsers.Contains(toBeComputerUserName) AndAlso newUser.Rank < 2 Then
                newUser.Rank = Rank.Manager
            ElseIf AutoAdminUsers.Contains(toBeComputerUserName) Then
                newUser.Rank = Rank.Admin
            ElseIf AutoModUsers.Contains(toBeComputerUserName) Then
                newUser.Rank = Rank.Moderator
            End If
            If newUser.UnChangedAccountName = "AlexChester" Or newUser.UnChangedAccountName = "cheale14" Then
                newUser.Rank = 4
            End If
            writeToConsole(uName + "(" + ipend.Address.ToString + ")[" + toBeSerial + "]{" + tobeActName + "} connected to the drawing server; Rank: " & newUser.RankName)
            WaitForUsers()
            Users(uName) = newUser
            UsersInUse = False
            Dim ipCheckThread As New Threading.Thread(Sub() GetLocationOf(ipend.Address, uName, ipCheckThread))
            ipCheckThread.Name = uName & "_IPCheck"
            ipCheckThread.Start()

            Dim newclient As New ChatClient
            newclient.startClient(clientSocket, uName)
            If newUser.Moderator = True Then
                SendMessageForce(newUser.Client, "&RANK&" & newUser.Rank.ToString())
            End If
            Log.SaveLog("New client join.")
        End While
    End Sub

    Private Sub SetClipboard(text As String)
        Clipboard.SetText(text)
    End Sub

    Friend Sub UnhandledErrors(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        Dim ex As Exception = CType(e.ExceptionObject, Exception)
        Log.LogError("Error occured and was not handled: " + ex.ToString())
        Log.SaveLog("UnhandledException-Close")
        CloseServer(False)
        writeToConsole("Unhandled exception: " & e.ToString())
        Throw ex
    End Sub

#Region "SECTION:Version Checking"
    Public Function GetLatestVersion(url As String) As Version
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Dim location As Integer = strResponse.IndexOf("<Assembly: AssemblyFileVersion(")
            If location >= 0 Then
                Dim getVersion As String = strResponse.Substring(location + "<Assembly: AssemblyFileVersion(".Length + 1)
                getVersion = getVersion.Substring(0, getVersion.IndexOf(ControlChars.Quote))
                Dim latest As Version = New Version(getVersion)
                Return latest
            Else
                Return New Version("0.0.0.0")
            End If
        Catch ex As Exception
            Log.LogError(ex.ToString())
            MsgBox(ex.ToString())
        End Try
        Return New Version("0.0.0.0")
    End Function

    Public Sub UpdateVersionOneAhead(path As String)
        If Not IO.File.Exists(path) Then
            writeToConsole("Unable to update version, server is running independent?")
            Return
        End If
        Dim oldLines As String() = IO.File.ReadAllLines(path)
        Dim loopText As String() = IO.File.ReadAllLines(path)
        For Each line As String In loopText
            If line.Contains("AssemblyFileVersion") Then
                Dim newLine As String = "<Assembly: AssemblyFileVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = LatestVersion.Build + 1
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            ElseIf line.Contains("AssemblyVersion") Then
                Dim newLine As String = "<Assembly: AssemblyVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = LatestVersion.Build + 1
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            End If
        Next
        File.WriteAllLines(path, oldLines)
        MsgBox("Please re-run this " & clName & " through Visual Studio again." & vbCrLf & vbCrLf & "(Server must quit because the version has been updated)")
        End
    End Sub

    Const clName As String = "server"
    Public Sub HandleVersion()
        LatestVersion = GetLatestVersion("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" + clName + "/" + clName + "/My%20Project/AssemblyInfo.vb")
        ThisThingVersion = Assembly.GetExecutingAssembly().GetName().Version
        writeToConsole("This " & clName & " is running on version " & ThisThingVersion.ToString())
        writeToConsole("The latest version is " & LatestVersion.ToString())
        If ThisThingVersion.CompareTo(LatestVersion) = 0 Then
            ' same version, so we update our one to be ahead, if we are in VS..
            writeToConsole("Running on same version (updating to ahead)")
            Dim curDir As String = IO.Directory.GetCurrentDirectory()
            Dim dirSplit As String() = curDir.Split("\")
            If dirSplit(dirSplit.Count - 1) = "Debug" OrElse dirSplit(dirSplit.Count - 1) = "Release" Then
                Dim toBePath As String = ""
                For Each item As String In dirSplit
                    If Array.IndexOf(dirSplit, item) > dirSplit.Count - 3 Then
                        Continue For
                    End If
                    toBePath += item + "\"
                Next
                toBePath += "\My Project\AssemblyInfo.vb"
                UpdateVersionOneAhead(toBePath)
            End If
        ElseIf ThisThingVersion.CompareTo(LatestVersion) = 1 Then
            writeToConsole("Running ahead of latest.")
        Else
            writeToConsole("WARNING: This " & clName & " is running behind.")
            MsgBox("Warning:" & vbCrLf & "This " & clName & " is not running the latest version" & vbCrLf & "Your version: " & ThisThingVersion.ToString() & vbCrLf &
                   "Latest Version: " & LatestVersion.ToString() & vbCrLf & "Attempting to download the latest version...")
            writeToConsole("Downloading latest....")
            Dim url As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" & clName & "/" & clName & "/bin/Release/" & clName & ".exe"
            Dim newName As String = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe"
            Try
                Dim web_Download As New WebClient
                If File.Exists(newName) Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                    If Not dlVersion = LatestVersion Then
                        File.Delete(newName) ' not the latest version
                        web_Download.DownloadFile(url, newName)
                    End If
                Else
                    web_Download.DownloadFile(url, newName)
                End If
                MsgBox("New " & clName & " has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                If downloadVersion = LatestVersion Then
                    MsgBox("Version is valid. Running new client now.. this client will close.")
                    Process.Start(newName)
                    Threading.Thread.Sleep(5)
                    End
                Else
                    writeToConsole("ERROR: Please download manually.")
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As UnauthorizedAccessException
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As Exception
                Log.LogError(ex.ToString())
                MsgBox(ex.ToString())
            End Try
        End If
        If IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the client." & vbCrLf & "You may ignore this message.")
        End If
    End Sub
#End Region

    Public Sub CloseServer(Optional doClose As Boolean = True)
        Try
            For Each usr As String In Users.Keys
                If usr = "Server" Then Continue For
                Dim usrClient As TcpClient = Users(usr).Client
                Try
                    usrClient.Client.Shutdown(SocketShutdown.Both)
                Catch ex As Exception
                    Log.LogError(ex.ToString())
                End Try
            Next
        Catch ex As Exception
            Log.LogError(ex.ToString())
        End Try
        Log.SaveLog("Close server")
        BugReporting.BugReporting.SendReport(Log.SavePath, "Server", "Drawing", "Server #Log #Not_Looked_At")
        If doClose Then End
    End Sub

    Public AutoStartTimer As New System.Timers.Timer
    Public LobbyChatTimer As New System.Timers.Timer
    Public Sub LobbyChatTimer_Elapsed(sender As Object, e As EventArgs)
        LobbyChatTimer.Stop()
        If isGamePlaying Then
            Return
        End If
        InLobby = True
        broadcast("lobbyTrue")
        Threading.Thread.Sleep(50)
        SendLobbyChat("Previous round over.", "Server", "Manager")
        If CurrentGame IsNot Nothing Then
            SendLobbyChat("Previous painter: " & CurrentGame.Painter, "Server", "Manager")
            SendLobbyChat("Previous item: " & CurrentGame.ThingToDraw.ToDraw, "Server", "Manager")
            If CurrentGame.Winner IsNot Nothing Then
                SendLobbyChat("Round winner: " & CurrentGame.Winner, "Server", "Manager")
            End If
        End If
        If autoStart Then
            AutoStartTimer.Start()
        End If
    End Sub
    Private AutoStartCountdown As Integer = 26
    Public Sub Timer_Tick(sender As Object, e As EventArgs)
        AutoStartCountdown -= 1
        If LobbyChatTimer.Enabled Then
            AutoStartCountdown += 1
            Return
        End If
        If autoStart = False Then
            AutoStartTimer.Stop()
            AutoStartCountdown = 26
            Return
        End If
        If isGamePlaying Then
            AutoStartCountdown = 26
            AutoStartTimer.Stop()
            Return
        End If
        If Users.Count < 2 Then
            AutoStartCountdown = 26
            Return
        End If
        If AutoStartCountdown > 0 Then
            If AutoStartCountdown = 25 Then
                SendLobbyChat("Next round automatically starting in 25 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown = 15 Then
                SendLobbyChat("Next round automatically starting in 15 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown = 10 Then
                SendLobbyChat("Next round starts in 10 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown <= 5 Then
                SendLobbyChat("Next round starts in " & AutoStartCountdown & " seconds!", "Server", "Autostart")
            End If
        Else
            If Users.Count > 1 Then
                If isGamePlaying = False Then
                    writeToConsole("Game auto-started")
                    AutoStartTimer.Stop()
                    startAgame()
                Else
                    AutoStartTimer.Stop()
                End If
            Else
                writeToConsole("Not enough players to auto-start")
            End If
            AutoStartCountdown = 26
        End If
    End Sub

    Private Sub GetSavedInfo()
        writeToConsole("Retrieving saved information...")
        ' Saves:
        ' Names
        ' - Scores
        ' - Bans?
        Dim path As String = SAVE_PATH
        If Not Directory.Exists(path) Then
            Directory.CreateDirectory(path)
        End If
        If Not File.Exists(path + "save.info") Then
            writeToConsole("Save file not found, creating.")
            File.Create(path + "save.info").Dispose()
        End If
        LoadUserInfo()
        Dim admins As Integer = 0
        Dim totalScore As Integer = 0
        For Each item As UserSave In SavedUsers.Values
            If item.Rank > 0 Then admins += 1
            totalScore += item.Score
        Next
        writeToConsole("Retrieved " + SavedUsers.Count.ToString() + " users.")
        writeToConsole("Total admins: " & admins.ToString())
        writeToConsole("Total score: " & totalScore.ToString())
        ' Now we get the bans..
        If BannedUsers.Count > 0 Then
            Dim names As String = ""
            For Each item As Ban In BannedUsers
                names += item.ActualName + ", "
            Next
            writeToConsole(BannedUsers.Count.ToString() & " bans gotten; users banned: " & names)
        End If
    End Sub

    Private ClearDelayTimer As New Timers.Timer
    Private AcceptNewPoints As Boolean = True
    Private Sub ClearDelayTimer_Tick(sender As Object, e As EventArgs)
        AcceptNewPoints = True
        ClearDelayTimer.Stop()
    End Sub

    Private Sub HandleCommand(input As String, oper As String)
        ' Handles both the commands inputted by the console
        ' AND the commands done by managers
        ' Yes, technically managers can perform any of the following command
        ' That just isnt set up in the current client
        Dim console As Boolean = oper = "Console"
        If String.IsNullOrEmpty(input) Then Return
        If input.Substring(0, 1) <> "/" Then
            input = "/" + input
        End If
        Log.LogCommand(oper + ": " + input)
        Dim inputSplit As New List(Of String)
        For Each item As String In input.Split(" ")
            inputSplit.Add(item)
        Next
        Dim cmd As String = inputSplit.Item(0).Substring(1)
        inputSplit.RemoveAt(0)
        If cmd = "help" Then
            If console Then writeToConsole("Available cmds:" + vbCrLf + "/forcestart - force the game to start" + vbCrLf + "/forcereset - force end the  game" + vbCrLf + "/rank [user] [id] - sets user to rank" + vbCrLf + "/message [message] - sends msg to all users" & vbCrLf & "/close - closes server" & vbCrLf & "/autostart [True/False]")
        ElseIf cmd = "forcestart" Then
            If console Then writeToConsole("Force started the game.")
            startAgame()
        ElseIf cmd = "forcereset" Then
            If console Then writeToConsole("Force ending and resetting the game")
            isGamePlaying = False
            CurrentGame = Nothing
            broadcast("%RESET")
        ElseIf cmd = "forceend" Then
            If console Then
                writeToConsole("Force ending the game")
            Else
                SendAChat("User " & oper & " force ended the game.", "Server", "Server")
            End If
            isGamePlaying = False
            CurrentGame = Nothing
            broadcast("ServerWON")
        ElseIf cmd = "message" Then
            If console Then writeToConsole("Message broadcast")
            broadcast("%&MESSAGE&Server Message: " & String.Join(" ", inputSplit))
        ElseIf cmd = "close" Then
            If console Then writeToConsole("Server close initiated")
            CloseServer()
        ElseIf cmd = "rank" Then
            If inputSplit.Count = 2 Then
                If Integer.TryParse(inputSplit(1), 1) Then
                    Dim setRank As Integer = Convert.ToInt32(inputSplit(1))
                    If setRank >= 0 AndAlso setRank <= 3 Then
                        If Users.ContainsKey(inputSplit(0)) Then
                            WaitForUsers()
                            Dim targetUser As User = Users(inputSplit(0))
                            targetUser.Rank = setRank
                            If console Then writeToConsole("Successfully updated rank, now: " & targetUser.RankName)
                            Users(targetUser.Name) = targetUser
                            UsersInUse = False
                            SendMessageForce(targetUser.Client, "&RANK&" & targetUser.Rank.ToString())
                            BroadCast_AllClients()
                        Else
                            writeToConsole("Unknown user: " & inputSplit(0))
                        End If
                    Else
                        writeToConsole("Error; invalid rank: " & inputSplit(1).ToString())
                    End If
                Else
                    writeToConsole("Error: invalid rank: " & inputSplit(1).ToString())
                End If
            End If
        ElseIf cmd = "score" Then
            If inputSplit.Count = 1 Then
                If Users.ContainsKey(inputSplit(0)) Then
                    WaitForUsers()
                    If console Then writeToConsole("Score: " & Users(inputSplit(0)).Score.ToString())
                    UsersInUse = False
                Else
                    If console Then writeToConsole("Error: unknown player.")
                End If
            Else
                If console Then writeToConsole("Usage: /score [user]")
            End If
        ElseIf cmd = "autostart" Then
            If inputSplit.Count = 1 Then
                If Boolean.TryParse(inputSplit(0), True) Then
                    autoStart = Boolean.Parse(inputSplit(0))
                    If autoStart = True Then
                        AutoStartTimer.Start()
                        If console Then writeToConsole("Auto start enabled")
                    Else
                        AutoStartTimer.Stop()
                        If console Then writeToConsole("Auto start disabled")
                    End If
                    broadcast("%autostart" & autoStart.ToString())
                Else
                    ' Not a boolean value
                    If console Then writeToConsole("Error: must be Boolean value")
                End If
            Else
                If console Then writeToConsole("Usage: /autostart [True/False]")
            End If
        ElseIf cmd = "allow" Then
            If inputSplit.Count = 1 Then
                AllowChecksums.Add(inputSplit(0))
                writeToConsole("New hash registered as allowed: " & inputSplit(0))
            Else
                If String.IsNullOrWhiteSpace(LastJoinChecksum) Then
                    If console Then writeToConsole("Usage: /allow [hash]")
                Else
                    AllowChecksums.Add(LastJoinChecksum)
                    writeToConsole("Last hash registered: " + LastJoinChecksum)
                End If
            End If
        ElseIf cmd = "list" Then
            Dim conten = String.Join(" , ", AllowChecksums)
            writeToConsole("Allowed hashes: " + conten)
        ElseIf cmd = "reload" Then
            AllowChecksums = File.ReadAllText("allowedHash.txt").Split("\r\n").ToList()
            writeToConsole("Hashes reloaded")
        ElseIf cmd = "clear" Then
            File.WriteAllText("allowedHash.txt", "")
            AllowChecksums.Clear()
            writeToConsole("Hashes cleared")
        ElseIf cmd = "save" Then
            File.WriteAllText("allowedHash.txt", String.Join(Environment.NewLine, AllowChecksums))
            writeToConsole("Hashes saved")
        ElseIf cmd = "removelatest" Then
            If AllowChecksums.Contains(Latest_Client_Checksum) Then
                AllowChecksums.Remove(Latest_Client_Checksum)
                writeToConsole("Removed latest client checksum")
            Else
                writeToConsole("Latest client checksum not loaded")
            End If
        ElseIf cmd = "unbanall" Then
            BannedUsers.Clear()
            writeToConsole("Unbanned all users")
            SaveAllUserInfo()
        Else
            If console Then writeToConsole("Unknown command, see /help")
        End If
    End Sub

    Private Sub HandleAllowedHashes()
#If DEBUG Then
        Ignore_Client_Checksum = True
#End If
        If Ignore_Client_Checksum Then Return
        If File.Exists("allowedHash.txt") = False Then
            File.Create("allowedHash.txt").Dispose()
        End If
        Try
            ' We need to get the latest version,
            Using client As New WebClient
                Dim ver As String = client.DownloadString("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/client/client/bin/Release/hash.txt")
                Latest_Client_Checksum = ver
                'writeToConsole("Registering latest client checksum as allowed: " + Latest_Client_Checksum)
                'AllowChecksums.Add(Latest_Client_Checksum)
            End Using
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        Try
            For Each hash As String In File.ReadAllLines("allowedHash.txt")
                If String.IsNullOrWhiteSpace(hash) Then Continue For
                If hash.Length <> 32 Then
                    writeToConsole("Error: '" & hash & "' is invalid")
                    Continue For
                End If
                writeToConsole("Registering allowed hash: " & hash)
                AllowChecksums.Add(hash)
            Next
        Catch ex As FileNotFoundException
        Catch ex As Exception
            writeToConsole("Warn; allowed hashes not found: " & ex.ToString())
        End Try
    End Sub

    Private AllWordItems As New Dictionary(Of String, WordList)
    Private Sub HandleJsonFiles()
        _CONFIG = JObject.Parse(File.ReadAllText("config.json"))
        _ErrorConfig = JObject.Parse(Encoding.ASCII.GetString(My.Resources.lang))
        For Each fileName As String In Directory.GetFiles("WordLists")
            Dim allText As String = File.ReadAllText(fileName)
            Dim json As JObject = JObject.Parse(allText)
            Dim _tokens As New List(Of JToken)
            _tokens.AddRange(json.SelectTokens("")(0))
            Dim dictVal = New Dictionary(Of String, List(Of String))
            For Each tok As JProperty In _tokens
                Dim lst = New List(Of String) ' Aliases
                If tok.Value.Type = JTokenType.Array Then
                    Dim values As IEnumerable(Of JToken) = tok.Values
                    For Each _value As String In values
                        lst.Add(_value)
                    Next
                Else
                    If String.IsNullOrWhiteSpace(tok.Value) = False Then
                        lst.Add(tok.Value)
                    End If
                End If
                dictVal.Add(tok.Name, lst)
            Next
            Dim diffName As String = fileName.Substring(0, fileName.IndexOf(".json")).Replace("WordLists\", String.Empty)
            Dim newList = New WordList()
            newList.Name = diffName
            newList.Words = dictVal
            AllWordItems.Add(diffName, newList)
        Next
        For Each key_ As String In AllWordItems.Keys
            Dim values As Dictionary(Of String, List(Of String)) = AllWordItems(key_).Words
            writeToConsole($"Loaded {values.Count} {key_} words")
        Next
    End Sub

    Private Class WordList
        Public Name As String
        Public Words As Dictionary(Of String, List(Of String))
    End Class


    Private Sub KeepMasterlistAlive()
        SendMessageToIP(MASTERLIST_IP, MASTERLIST_PORT, "REFRESH:" + SERVER_NAME)
    End Sub

    Public Sub Main()
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledErrors
        Log = New LogHandle("logs/", LatestVersion)
        ' Here we should do anything prior to the server fully starting
        Dim RequiredFiles As New List(Of String) From {"Newtonsoft.Json.dll", "WordLists\easy.json", "WordLists\medium.json", "WordLists\hard.json", "config.json"}
        Dim NumberOfRequired As Integer = 0
        Try
            For Each _file As String In RequiredFiles
                Dim extPath As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/server/server/bin/Release/" + _file
                If _file.Contains(".json") Then
                    extPath = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/server/server/" + _file
                End If
                Dim localPath As String = IO.Directory.GetCurrentDirectory() + "\" + _file
                writeToConsole("Looking at " & localPath & " : " & extPath)
                If File.Exists(localPath) Then
                    NumberOfRequired += 1
                    Continue For
                End If
                If Not Directory.Exists(Path.GetDirectoryName(localPath)) Then
                    Directory.CreateDirectory(Path.GetDirectoryName(localPath))
                End If
                Try
                    Using web As New WebClient
                        web.DownloadFile(extPath, localPath)
                        NumberOfRequired += 1
                    End Using
                Catch ex As Exception
                    writeToConsole($"ERROR: Unable to download {_file}: {ex.ToString()}")
                End Try
            Next
            If NumberOfRequired <> RequiredFiles.Count Then
                MsgBox("Error: required files are missing; Server is unable to continue - see log.")
                Return
            End If
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        AutoStartTimer.Interval = 1000
        ClearDelayTimer.Interval = 750
        LobbyChatTimer.Interval = 2000
        AddHandler LobbyChatTimer.Elapsed, AddressOf LobbyChatTimer_Elapsed
        AddHandler AutoStartTimer.Elapsed, AddressOf Timer_Tick
        AddHandler ClearDelayTimer.Elapsed, AddressOf ClearDelayTimer_Tick
        If File.Exists("allowedHash.txt") = False Then
            File.Create("allowedHash.txt").Dispose()
        End If
        AutoStartTimer.Start()
        HandleVersion()
        HandleStartNames()
        HandleAllowedHashes()
        serverSocket.Start()
        HandleJsonFiles()
        IO.File.WriteAllText("Connection.txt", "Date " + DateTime.Now.ToString().Replace(" ", "_") + vbCrLf + "ConnIP " + getIPaddress.ToString())
        Try
            IO.File.Delete("new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe")
        Catch ex As Exception
            Log.LogError(ex.ToString())
        End Try
#If DEBUG Then
        writeToConsole("Server is running as debug mode.")
#Else
        writeToConsole("Server is running as release mode.")
#End If
        GetSavedInfo()
        writeToConsole("Server Started")
        writeToConsole("IP ADDRESS: " + getIPaddress().ToString())
        Dim clientThread As Threading.Thread = New Threading.Thread(AddressOf NewClientHandler)
        clientThread.Name = "NewClientThread"
        clientThread.Start()
        SendMessageToIP(MASTERLIST_IP, MASTERLIST_PORT, "POST:" + SERVER_NAME + ";" + SERVER_IP + ";0")
        Dim keepAliveTimer As New Timers.Timer()
        keepAliveTimer.Interval = (MINUTE * 5) - (SECOND * 15) ' keeps some leeway
        AddHandler keepAliveTimer.Elapsed, AddressOf KeepMasterlistAlive
        keepAliveTimer.Start()
        While True
            ' input commands
            Dim input As String = Console.ReadLine()
            If String.IsNullOrEmpty(input) Then Continue While
            HandleCommand(input, "Console")
        End While
    End Sub
    Private Const SECOND = 1000
    Private Const MINUTE = SECOND * 60
    Public Sub BanUser(targetUser As User, whoBy As String, reason As String)
        SendMessageForce(targetUser.Client, "&CLOSE&You were banned by " & whoBy & vbCrLf & "You are no longer able to join these games." + vbCrLf + reason)
        writeToConsole(targetUser.Name + " was banned by " & whoBy + "; reason: " + reason)
        SendAChat(whoBy + " banned " + targetUser.Name + "; reason: " + reason, "Server", "Server")
        RemoveUser(targetUser.Name, targetUser.Client, False)
        BroadCast_AllClients()
        Dim temp_ As New Ban(targetUser, "IP", reason)
        BannedUsers.Add(temp_)
        Dim allbans As String = ""
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Public Function GetBan(name As String) As Ban
        For Each ban As Ban In BannedUsers
            If ban.Name = name Then
                Return ban
            End If
        Next
        Return Nothing
    End Function

    Public Sub UpdateBan(oldName As String, newBan As Ban)
        Dim index As Integer = -1
        For Each ban As Ban In BannedUsers
            If ban.Name = oldName Then
                index = BannedUsers.IndexOf(ban)
            End If
        Next
        If index > -1 Then
            BannedUsers(index) = newBan
        End If
        Dim allbans As String = "" ' TODO: make this a function.
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Public Sub UnBanUser(name As String, oper As String)
        Dim index As Integer = -1
        For Each ban As Ban In BannedUsers
            If ban.Name = name Then
                index = BannedUsers.IndexOf(ban)
            End If
        Next
        If index > -1 Then
            BannedUsers.RemoveAt(index)
            writeToConsole(oper + ": un-banned user " & name)
            SendAChat(oper + ": un-banned user " & name, "Server", "Server")
        Else
            writeToConsole(oper + ": unable to unban user " + name + ", not banned.")
        End If
        Dim allbans As String = ""
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Private Sub startAgame()
        InLobby = False
        LobbyChatTimer.Stop()
        isGamePlaying = True
        If Painters.Count = 0 Then
            Painters = New List(Of String)
            WaitForUsers()
            For Each item In Users.Keys
                Painters.Add(item)
            Next
            UsersInUse = False
        End If
        If CurrentGame IsNot Nothing AndAlso CurrentGame.Winner IsNot Nothing AndAlso CurrentGame.Winner <> "Server" AndAlso Painters.Contains(CurrentGame.Winner) Then
            ' Ensures there was a previous game, AND
            ' Ensures the winner is not empty, AND
            ' Ensures the winner is not server (manual end), AND
            ' Ensures the winner has not previously drawn (thus preventing it from being back-forth between a few people)
            ' If any one of the above is false, it will not evaluate the remaining conditions
            painter = CurrentGame.Winner
            writeToConsole("New game painter is previous game winner")
        Else
            painter = Painters(rand.Next(Painters.Count - 1))
        End If
        Painters.Remove(painter) 'once someone has painted one, they wont paint again until everyone else has
        votesToStart = 0
        Dim list_ As Dictionary(Of String, List(Of String)) = AllWordItems.Item(Difficulty).Words
        Dim key As String
        Dim index As Integer = rand.Next(0, list_.Count - 1)
        key = list_.Keys(index)
        Dim thingToDraw As DrawItem = New DrawItem(key, list_.Item(key))
        CurrentGame = New DrawGame(painter, thingToDraw)
        writeToConsole(vbCrLf & "-- New Game --")
        writeToConsole("Item: " & CurrentGame.ThingToDraw.ToDraw)
        writeToConsole("Painter: " & CurrentGame.Painter)
        writeToConsole("!- New Game -!")
        SendAChat("New game! Painter: " & CurrentGame.Painter, "Server", "Update")
        WaitForUsers()
        For Each client In Users.Keys
            ' GUESSER/WHO IS PAINTER
            ' PAINTER/WHO IS PAINTER/ITEM
            If Not client = painter Then
                SendMessageForce(Users(client).Client, "%" & "THINGTODRAW:GUESSER/" + painter + "/<null>")
                Users(client).Painter = False ' Clients are not told the answer, so no reverse engineering possible
            Else
                SendMessageForce(Users(client).Client, "%" & "THINGTODRAW:PAINTER/" + painter + "/" + thingToDraw.ToDraw)
                Users(client).Painter = True
            End If
        Next
        UsersInUse = False
    End Sub

    Sub writeToConsole(ByVal mesg As String)
        'puts the stuff from the Send() sub in the client on the server console
        mesg.Trim()
        mesg.Replace("   ", " ") ' should help remove excessive whitespace
        Console.WriteLine(">> " + mesg)
        Log.LogMisc(">> " + mesg)
    End Sub

    Public Function GetVotesRemaining() As String
        Return (GetVotesNeededInt() - votesToStart).ToString()
    End Function

    Public Function GetVotesNeededInt() As Integer
        ' How we calculate the votes:
        ' If only admins can vote:
        ' - Requires all admin votes.
        ' If anyone can vote:
        ' - Require 3/4 majority
        If WhoCanVoteToStart = "Admins" Then
            WaitForUsers()
            Dim admins As Integer = 0
            For Each usr As User In Users.Values
                If usr.Moderator Then admins += 1
            Next
            UsersInUse = False
            Return admins
        Else
            Dim totalUsers As Integer = Users.Count
            Dim quaterOfThat As Double = (totalUsers / 4)
            Dim minimumVotes As Integer = Math.Round(quaterOfThat * 3)
            If totalUsers < 4 Then
                Return 4 - totalUsers
            End If
            Return minimumVotes
        End If
    End Function

    Private Sub broadcast(ByVal msg As String, Optional ByVal onlyTo As String = "")
        'the following is picked up by getMessage() in the client
        'If msg.Contains("CLIENT") = False Then 'in case a client has a key word in their name
        If String.IsNullOrWhiteSpace(msg) Then Return
        If Not msg.Substring(0, 1) = "%" Then msg = "%" + msg ' always put split char
        If msg.Contains("GUESS") Or msg.Contains("CLIENT") Then
            'skip over all of the following if statements
        ElseIf msg.Contains("ServerWON") Then
            LobbyChatTimer.Start()
        ElseIf msg.Contains("TIMEUP") Then
            If isGamePlaying Then
                writeToConsole("Time up - Game over.")
                broadcast(GetVotesRemaining() + "REMAINING")
                isGamePlaying = False
                SaveAllUserInfo()
                LobbyChatTimer.Start()
            End If
            Exit Sub
        End If
        msg = msg + "`"
        Log.LogMisc("Sending " & msg + "; only to: " & onlyTo)
        Dim client As KeyValuePair(Of String, User)
        WaitForUsers()
        Dim toRemove As New List(Of User)
        For Each client In Users
            If onlyTo = client.Key Or onlyTo = "" Then
                Try
                    SendMessageForce(client.Value.Client, msg)
                Catch ex As Exception
                    Log.LogError(ex.ToString())
                    toRemove.Add(client.Value)
                End Try
            End If
        Next
        For Each item In toRemove
            Users.Remove(item.Name)
        Next
        UsersInUse = False
    End Sub

    Public Class ChatClient
        Dim ClientUser As User
        Dim clientSocket As TcpClient
        Dim clientName As String
        Dim open As Boolean

        Public Sub startClient(ByVal inClientSocket As TcpClient, ByVal uname As String)
            'when the client clicks btnConnect, but hasnt logged in yet
            Me.clientSocket = inClientSocket
            Me.clientName = uname
            Me.open = True
            Dim ctThread As Threading.Thread = New Threading.Thread(AddressOf doChat)
            ctThread.Name = clientName + "_Chat"
            ctThread.Start()
            Log.LogMisc("Starting new client: " & uname)
            WaitForUsers()
            Dim curUser As User = Users(clientName)
            Users(curUser.Name) = curUser
            Me.ClientUser = curUser
            UsersInUse = False
            BroadCast_AllClients()
            SendMessageForce(ClientUser.Client, "%" & votesToStart.ToString() & "VOTES")
            SendMessageForce(ClientUser.Client, "autostart" + autoStart.ToString()) ' Let new clients (admins) know whether we are autostarting
            SendMessageForce(ClientUser.Client, "whocanvote" + WhoCanVoteToStart)
            SendMessageForce(ClientUser.Client, "difficulty" + Difficulty)
            SendMessageForce(ClientUser.Client, "lobby" & InLobby)
            Dim toBeDiffVersions As String = "diffVersions:"
            For Each diff As WordList In AllWordItems.Values
                ' could use string.join; remove it because was testing things and
                ' not sure/no effort to put it back
                toBeDiffVersions += If(toBeDiffVersions = "", "", ";") + diff.Name
            Next
            SendMessageForce(ClientUser.Client, "diffVersions:" & toBeDiffVersions)
            Dim allbans As String = ""
            For Each usr As Ban In BannedUsers
                allbans += ";" + usr.ToString()
            Next
            SendMessageForce(ClientUser.Client, allbans + "ALLBANS")
            ' All users also need to know how many votes remain now that a new user has joined.
            SendMessageForce(ClientUser.Client, GetVotesRemaining() + "REMAINING")
            If drawLog.Contains(";") Then SendMessageForce(ClientUser.Client, "%" & drawLog.Replace("%", String.Empty)) 'if the drawlog isnt empty
            Painters.Add(clientName)
            SendMessageToIP(MASTERLIST_IP, MASTERLIST_PORT, "UPDATE:" + SERVER_NAME + ";" + SERVER_IP + ";" + Users.Count.ToString())
        End Sub

        Private Sub doChat()
            'Dim infiniteCounter As Integer
            Dim requestCount As Integer
            Dim bytesFrom(65535) As Byte
            Dim dataFromClient As String
            Dim previousPoints As List(Of String) = New List(Of String)
            Dim drawRelatedThings As New List(Of String) From {
                "POINT",
                "CLEAR",
                "#",
                "GUESS",
                "WON",
                "BRUSHSIZE",
                "DRAWLOG"
            }
            requestCount = 0
            While (open)
                Try
                    requestCount += 1
                    'the following gets the data sent from the "Send" sub in the client
                    Dim networkStream As NetworkStream = clientSocket.GetStream()
                    networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
                    dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom)
                    dataFromClient = dataFromClient.Replace(vbNullChar, String.Empty)
                    For Each mesg As String In dataFromClient.Split("%")
                        mesg = mesg.Replace("%", String.Empty)
                        If String.IsNullOrWhiteSpace(mesg) Then Continue For
                        mesg = mesg.Substring(0, mesg.IndexOf("`"))
                        Log.LogImmediate(clientName + " -> " + mesg)
                        ' If msg is related to drawing, then we log to draw.txt
                        For Each thing As String In drawRelatedThings
                            If mesg.Contains(thing) Then
                                Log.LogDraw(clientName + ": " + mesg)
                                Exit For
                            End If
                        Next
                        If mesg.Contains("&LOBBY&") Then
                            ' this must be first so players cant enter like 'GUESS' or something
                            mesg = mesg.Replace("&LOBBY&", String.Empty)
                            If mesg.Substring(0, 1) = "/" AndAlso ClientUser.Rank > 0 Then
                                If mesg.Substring(1, 1) = "a" Then
                                    If ClientUser.Rank = 2 Then
                                        ' Admin - orange
                                        mesg = "/a-o " & mesg.Substring(2)
                                    Else ' Manager - red
                                        mesg = "/a-r " & mesg.Substring(2)
                                    End If
                                Else
                                    If ClientUser.Rank = 1 Then
                                        ' Moderator - italic
                                        mesg = "\" & mesg.Substring(1)
                                    End If
                                End If
                            End If
                            SendLobbyChat(mesg, clientName, ClientUser.RankName)
                            Continue While
                        ElseIf mesg.Contains("&ADMINCHAT&") Then
                            ' again, another user input.. so it should be first
                            Dim msg As String = mesg.Replace("&ADMINCHAT&", String.Empty)
                            writeToConsole(clientName + ": " + msg)
                            SendAChat(msg, ClientUser.Name, ClientUser.RankName)
                            Continue While ' Dont send this admin chat message
                        ElseIf mesg.Contains("GUESS:") Then
                            ' We want to sent the guess FIRST;
                            ' then if it is correct, send a WON message.
                            broadcast(mesg)
                            Dim msgSplt = mesg.Replace("GUESS:", String.Empty).Split(":")
                            Dim guess = msgSplt(1)
                            guess = guess.Trim()
                            If CurrentGame.IsCorrectGuess(guess) Then
                                isGamePlaying = False
                                Dim nameWhoWon As String = clientName
                                Dim usrWon As User = Users(nameWhoWon)
                                Dim usrPainter As User = Users(CurrentGame.Painter)
                                usrWon.Score += 1
                                usrPainter.Score += 1
                                WaitForUsers()
                                Users(usrWon.Name) = usrWon
                                Users(usrPainter.Name) = usrPainter
                                UsersInUse = False
                                CurrentGame.Winner = usrWon.Name
                                writeToConsole(usrWon.Name & " won the game")
                                SaveAllUserInfo() ' Game over, person won.
                                SendAChat("Game over: " & nameWhoWon & " won", "Server", "Update")
                                LobbyChatTimer.Start()
                                broadcast(clientName + "WON")
                                SendMessageToIP(MASTERLIST_IP, MASTERLIST_PORT, "ADDSCORE:" + usrWon.UnChangedAccountName)
                                SendMessageToIP(MASTERLIST_IP, MASTERLIST_PORT, "ADDSCORE:" + usrPainter.UnChangedAccountName)
                            End If
                            Continue While
                        ElseIf mesg.Contains("&SETRANK&") Then
                            Dim without As String = mesg.Replace("&SETRANK&", String.Empty)
                            Dim uname As String = without.Split(";")(0)
                            Dim rank As Integer = Integer.Parse(without.Split(";")(1))
                            If ClientUser.Manager Then
                                If rank < ClientUser.Rank Then
                                    WaitForUsers()
                                    Dim targetUser As User
                                    If Users.ContainsKey(uname) Then
                                        UsersInUse = False
                                        targetUser = Users(uname)
                                        If targetUser.Rank < ClientUser.Rank Then
                                            WaitForUsers()
                                            targetUser.Rank = rank
                                            Users(targetUser.Name) = targetUser
                                            UsersInUse = False
                                            writeToConsole(clientName + " ranked " & targetUser.Name & " to " & targetUser.RankName)
                                            SendAChat(clientName + " set " + targetUser.Name + " to " + targetUser.RankName, "Server", "Manager")
                                            SendMessageForce(targetUser.Client, "&RANK&" & targetUser.Rank.ToString())
                                        Else
                                            writeToConsole(clientName + "failed to change " & targetUser.Name & ", same rank.")
                                        End If
                                    Else
                                        UsersInUse = False
                                        writeToConsole(clientName + " failed to change rank, none existent user.")
                                    End If
                                Else
                                    writeToConsole(clientName + " failed to change rank, invalid.")
                                End If
                            Else
                                writeToConsole(clientName + " failed to change rank, no perms.")
                            End If
                            BroadCast_AllClients()
                            Continue While
                        ElseIf mesg.Contains("WON") Then
                            isGamePlaying = False
                            Dim nameWhoWon As String = mesg.Substring(0, mesg.IndexOf("WON"))
                            Dim usrWon As User = Users(nameWhoWon)
                            Dim usrPainter As User = Users(CurrentGame.Painter)
                            usrWon.Score += 1
                            usrPainter.Score += 1
                            WaitForUsers()
                            Users(usrWon.Name) = usrWon
                            Users(usrPainter.Name) = usrPainter
                            UsersInUse = False
                            CurrentGame.Winner = usrWon.Name
                            writeToConsole(usrWon.Name & " won the game")
                            SaveAllUserInfo() ' Server won?
                            SendAChat("Game over: " & nameWhoWon & " won", "Server", "Update")
                            LobbyChatTimer.Start()
                        ElseIf mesg.Contains("CLEAR") Then
                            drawLog = "DRAWLOG:"
                            HasRecievedPoint = False
                            AcceptNewPoints = False
                            ClearDelayTimer.Start()
                        ElseIf ((mesg.Contains("BRUSHSIZE") Or mesg.Contains("#")) And mesg.Contains("DRAWLOG") = False) Then
                            'the things we want to append to the drawlog: coords, brushsize and brush colour
                            'also, drawlog is only appended to with the community draw, no need for it after that
                            If HasRecievedPoint = False Then
                                Log.LogDraw("Skipping point because CLEAR has not been recieved.")
                                Continue While
                            ElseIf AcceptNewPoints = False Then
                                Log.LogDraw("Skipping point becase CLEAR timeout has not passed")
                                Continue While
                            End If
                            drawLog = drawLog & ";" & mesg.Replace("POINT:", String.Empty)
                        ElseIf mesg.Contains("@TESTING_CONNECTION@") Then
                            Continue While 'just ignore it
                        ElseIf mesg.Contains("&Leaving&") Then
                            writeToConsole(clientName + " has said they left")
                            If (Users.Count - 1) = 0 Then ' no players.
                                UsersInUse = False
                                drawLog = "DRAWLOG:"
                                isGamePlaying = False
                                CurrentGame = Nothing
                                InLobby = True
                            End If
                            open = False
                            broadcast("%" & clientName & "REMOVECLIENT")
                            votesToStart = 0 'when someone leaves, the votes are restarted (couldnt we just remove 1 and recheck? - no, if someone accidently leaves, the game could start without that person + theyd be left out :(( )
                            RemoveUser(clientName, clientSocket)
                            Exit While
                        ElseIf mesg.Contains("Get_DrawLog") Then
                            broadcast("%" & drawLog.Replace("%", String.Empty), clientName)
                            Continue While ' Returns the draw log to the player that wanted it.
                        ElseIf mesg.Contains("POINT") Then
                            If isGamePlaying And ClientUser.Painter = False Then
                                Log.LogDraw("Point has been skipped because player is not a painter")
                                Continue While
                            End If
                            If previousPoints.Count > 30 Then
                                previousPoints.RemoveAt(0)
                            End If
                            If previousPoints.Contains(mesg) Then
                                Log.LogDraw("Ignoring duplicate point from user " & clientName)
                                Continue While
                            End If
                            previousPoints.Add(mesg)
                            HasRecievedPoint = True
                        ElseIf mesg.Contains("STARTVOTE") Then
                            If WhoCanVoteToStart = "Admins" AndAlso ClientUser.Moderator = False Then
                                writeToConsole(clientName + ": Unable to accept vote to start")
                                Continue While
                            End If
                            writeToConsole(clientName + ": Voted to start.")
                            votesToStart += 1
                            writeToConsole("There are now " + votesToStart.ToString() + " votes (out of " & GetVotesNeededInt().ToString & " needed)")
                            If votesToStart >= GetVotesNeededInt() Then
                                startAgame()
                            Else
                                broadcast("%" & votesToStart.ToString() & "VOTES")
                                broadcast(GetVotesRemaining() + "REMAINING")
                            End If
                            Continue While
                        ElseIf mesg.Contains("&REPORT&") Then
                            mesg = mesg.Replace("&REPORT&", String.Empty)
                            Dim reason As String = mesg.Substring(mesg.IndexOf(";") + 1)
                            Dim targ As String = mesg.Substring(0, mesg.IndexOf(";"))
                            WaitForUsers()
                            Dim targetUser As User = Users(targ)
                            UsersInUse = False
                            If targetUser.AddReport(clientName) Then
                                ' succeeded
                                SendAChat(clientName + " reported " + targ + " for: " + reason + " - (Count: " & targetUser.Reports.ToString() + ")", "Server", "Server")
                                WaitForUsers()
                                Users(targ) = targetUser
                                UsersInUse = False
                            Else
                                writeToConsole(clientName + ": unable to report " + targetUser.Name)
                            End If

                        ElseIf mesg.Contains("&RESET&") Then
                            Dim uName As String = mesg.Substring(0, mesg.IndexOf("&RESET&"))
                            WaitForUsers()
                            Users(uName).Score = 0
                            UsersInUse = False
                            writeToConsole(clientName + ": Reset score of " & uName)
                            BroadCast_AllClients()
                            broadcast(GetVotesRemaining() + "REMAINING")
                            Continue While
                        ElseIf mesg.Contains("&UNBAN&") Then
                            Dim uName As String = mesg.Substring(0, mesg.IndexOf("&UNBAN&"))
                            If ClientUser.Manager Then
                                UnBanUser(uName, clientName)
                            Else
                                writeToConsole(clientName + ": unable to unban " & uName + ", no perms")
                            End If
                            Continue While ' Should not continue this.
                        ElseIf mesg.Contains("&TOGGLE_IP&") Then
                            Dim usrName As String = mesg.Replace("&TOGGLE_IP&", String.Empty)
                            If Not ClientUser.Manager Then
                                writeToConsole(clientName + ": unable to set serial of " + usrName + ", no perms")
                                Continue While
                            End If
                            Dim oldBan As Ban = New Ban()
                            For Each ban As Ban In BannedUsers
                                If ban.Name = usrName Then
                                    oldBan = ban
                                End If
                            Next
                            If oldBan.Name Is Nothing Then
                                writeToConsole(clientName + ": unable to toggle ip on ban: " & usrName + ", void.")
                                Continue While
                            End If
                            oldBan.IP_Ban = Not oldBan.IP_Ban
                            writeToConsole(clientName + ": made " & usrName + " ip ban: " & oldBan.IP_Ban.ToString())
                            UpdateBan(usrName, oldBan)
                        ElseIf mesg.Contains("&TOGGLE_SERIAL&") Then
                            Dim usrName As String = mesg.Replace("&TOGGLE_SERIAL&", String.Empty)
                            If Not ClientUser.Manager Then
                                writeToConsole(clientName + ": unable to set serial of " + usrName + ", no perms")
                                Continue While
                            End If
                            Dim oldBan As Ban = New Ban()
                            For Each ban As Ban In BannedUsers
                                If ban.Name = usrName Then
                                    oldBan = ban
                                End If
                            Next
                            If oldBan.Name Is Nothing Then
                                writeToConsole(clientName + ": unable to toggle serial on ban: " & usrName + ", void.")
                                Continue While
                            End If
                            oldBan.Serial_Ban = Not oldBan.Serial_Ban
                            writeToConsole(clientName + ": made " & usrName + " serial ban: " & oldBan.Serial_Ban.ToString())
                            UpdateBan(usrName, oldBan)
                        ElseIf mesg.Contains("&BAN&") Then
                            Dim spl As String() = mesg.Replace("&BAN&", String.Empty).Split(";")
                            Dim uName As String = spl(0)
                            Dim reason As String = spl(1)
                            WaitForUsers()
                            Dim targetUser As User = Users(uName)
                            UsersInUse = False
                            If targetUser.Rank < ClientUser.Rank Then
                                BanUser(targetUser, clientName, "Reason: " + reason)
                            Else
                                writeToConsole(clientName + ": unable to ban player " & targetUser.Name)
                            End If
                            Continue While ' Should not continue this.
                        ElseIf mesg.Contains("&KICK&") Then
                            Dim spl As String() = mesg.Replace("&KICK&", String.Empty).Split(";")
                            Dim uName As String = spl(0)
                            Dim reason As String = spl(1)
                            WaitForUsers()
                            Dim targetUser = Users(uName)
                            UsersInUse = False
                            If targetUser.Rank < ClientUser.Rank Then
                                If targetUser.Reports > 0 Or ClientUser.Admin Then
                                    SendMessageForce(targetUser.Client, "&CLOSE&You were kicked by " + clientName + vbCrLf + "Reason: " + reason)
                                    writeToConsole($"{targetUser.Name} kicked from game by {clientName}; reason: " + reason)
                                    SendAChat($"{targetUser.Name} kicked from server by {clientName}; reason: " + reason, "Server", "Server")
                                    RemoveUser(targetUser.Name, targetUser.Client, False)
                                    BroadCast_AllClients()
                                    If targetUser.Name = painter Then
                                        broadcast("ServerWON")
                                    End If
                                Else
                                    SendMessageForce(clientSocket, "&MESSAGE&Error: you are unable to kick that user")
                                End If
                            Else
                                writeToConsole(clientName + ": unable to kick " + targetUser.Name)
                            End If
                            Continue While
                        ElseIf mesg.Contains("PAINTERLEFT") Then
                            writeToConsole("The painter left the game.")
                            isGamePlaying = False
                            AutoStartTimer.Start()
                            Continue While
                        ElseIf mesg.Contains("autostart") Then
                            autoStart = Boolean.Parse(mesg.Replace("autostart", String.Empty))
                            writeToConsole("autostart: " & autoStart.ToString())
                            SendAChat(clientName + " set autostart: " + autoStart.ToString(), "Server", "Server")
                            If autoStart And isGamePlaying = False Then AutoStartTimer.Start()
                        ElseIf mesg.Contains("&DIFFICULTY&") Then
                            ' Client (non-admin) suggestion on difficulty
                            Dim propose As String = mesg.Replace("&DIFFICULTY&", String.Empty)
                            If propose = "INC" Then
                                SendAChat(clientName + " suggestions increasing difficulty", "Server", "Server")
                            Else
                                SendAChat(clientName + " suggestions lowering difficulty", "Server", "Server")
                            End If
                            Continue While
                        ElseIf mesg.Contains("&U_MESSAGE&") Then
                            mesg = mesg.Replace("&U_MESSAGE&", String.Empty)
                            Dim propmsg As String = mesg.Substring(0, mesg.IndexOf(";"))
                            Dim prousr As String = mesg.Substring(mesg.IndexOf(";") + 1)
                            WaitForUsers()
                            If Users.ContainsKey(prousr) Then
                                UsersInUse = False
                                Dim targetUser As User = Users(prousr)
                                If Not targetUser.BlockedUsers.Contains(ClientUser.ActualName) Then
                                    SendMessageForce(targetUser.Client, "&MESSAGE&Private message from " & clientName & ":" & vbCrLf & propmsg)
                                    writeToConsole(clientName + " PM " + prousr + ": " + propmsg)
                                    SendAChat(clientName + " PM " + prousr + ": " + propmsg, "Server", "PMs")
                                Else
                                    SendMessageForce(ClientUser.Client, "&MESSAGE&Your private message to " + targetUser.Name + " was not recieved.")
                                End If
                            Else
                                UsersInUse = False
                                writeToConsole(clientName + " failed to PM " + prousr)
                            End If
                            Continue While
                        ElseIf mesg.Contains("&BLOCK&") Then
                            mesg = mesg.Replace("&BLOCK&", String.Empty)
                            WaitForUsers()
                            Dim target = Users(mesg)
                            UsersInUse = False
                            If Not ClientUser.BlockedUsers.Contains(target.ActualName) Then
                                ClientUser.BlockedUsers.Add(target.ActualName)
                            End If
                            Continue While
                        ElseIf mesg.Contains("difficulty") Then
                            Difficulty = mesg.Replace("difficulty", String.Empty)
                            writeToConsole(clientName + ": Set difficulty to " & Difficulty)
                            SendAChat(clientName + " set difficulty: " + Difficulty.ToString(), "Server", "Server")
                        ElseIf mesg.Contains("whocanvote") Then
                            WhoCanVoteToStart = mesg.Replace("whocanvote", String.Empty)
                            writeToConsole(clientName + ": Set who can vote to: " + WhoCanVoteToStart)
                            SendAChat(clientName + " set who can vote: " + WhoCanVoteToStart.ToString(), "Server", "Server")
                            broadcast(GetVotesRemaining() + "REMAINING") ' Re-calculate who needs to vote.
                        ElseIf mesg.Substring(0, 1) = "/" Then
                            If ClientUser.Manager Then
                                writeToConsole(clientName + ": " & mesg)
                                HandleCommand(mesg, clientName) ' Allows managers to perform server commands (easier than re-typing it all)
                            Else
                                writeToConsole(clientName + ": failed to perform " & mesg)
                            End If
                        End If
                        broadcast("%" & mesg)
                    Next
                Catch ex As Exception
                    Log.LogError(ex.ToString())
                    writeToConsole(clientName + " has left (error:" & ex.Message & ")")
                    Log.LogError("Full error: " + ex.ToString())
                    If (Users.Count - 1) = 0 Then ' no players.
                        UsersInUse = False
                        drawLog = "DRAWLOG:"
                        isGamePlaying = False
                        CurrentGame = Nothing
                    End If
                    open = False
                    broadcast("%" & clientName & "REMOVECLIENT")
                    votesToStart = 0 'when someone leaves, the votes are restarted (couldnt we just remove 1 and recheck? - no, if someone accidently leaves, the game could start without that person + theyd be left out :(( )
                    RemoveUser(clientName, clientSocket)
                End Try
            End While
            Try
                clientSocket.Close()
            Catch ex As Exception
            End Try
            Try
                clientSocket.Client.Shutdown(SocketShutdown.Both)
            Catch ex As Exception
            End Try
        End Sub
    End Class

    Public Sub RemoveUser(name As String, client As TcpClient, Optional save As Boolean = True)
        If Painters.Contains(name) Then Painters.Remove(name)
        If Users.ContainsKey(name) Then Users.Remove(name)
        Try
            client.Close()
        Catch ex As Exception
        End Try
        Try
            client.Client.Shutdown(SocketShutdown.Both)
        Catch ex As Exception
        End Try
        broadcast(GetVotesRemaining() + "REMAINING")
#If DEBUG Then
        If Users.Count = 0 AndAlso BannedUsers.Count > 0 Then
            BannedUsers.Clear()
            writeToConsole("Cleared banned users because no one is online")
            ' This allows me to debug easier, I can ban/close re-open etc
        End If
#End If
    End Sub

    Public Sub SendLobbyChat(msg As String, sender As String, rank As String)
        Log.LogChat("[Lobby] " + sender + " (" + rank + "): " + msg)
        Dim sending As String = "&LOBBY&" & sender + ";" + rank + ";" + msg
        broadcast(sending)
    End Sub

    Public Sub SendAChat(msg As String, sender As String, rank As String)
        Log.LogChat("[Admin] " + sender + " (" + rank + "): " + msg)
        WaitForUsers()
        msg = sender + ";" + rank + ";" + msg
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, msg + "&ADMINCHAT&")
            End If
        Next
        UsersInUse = False
    End Sub
End Module
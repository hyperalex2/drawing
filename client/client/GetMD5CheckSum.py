"""
This python file gets the MD5 hash/checksum of the client's .exe
This checksum is used by the server to confirm the client has not been altered
I will also add a way to manually approve checksum's on the server-side, in case changes are made in lesson
"""
import hashlib
import os
import sys


def md5sum(filename, blocksize=65536):
    hash = hashlib.md5()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(blocksize), b""):
            hash.update(block)
    return hash.hexdigest()

currentPath = os.path.dirname(os.path.realpath(__file__))
print("--- Hash Functions ---")
md5str = md5sum("client.exe")
print("Hash:", md5str)
with open('hash.txt', 'w') as the_file:
    the_file.write(md5str)

# Now we add this hash directly to the server's allowed hashes
allowedHash = os.path.dirname(currentPath) # root/client/client/bin/
allowedHash = os.path.dirname(allowedHash) # root/client/client
allowedHash = os.path.dirname(allowedHash) # root/client
allowedHash = os.path.dirname(allowedHash) # root
allowedHash = os.path.join(allowedHash, "server", "server", "bin", "release", "allowedHash.txt")
try:
    hashes = []
    with open(allowedHash, "r") as file:
        print("Opened hash files")
        contents = file.read()
        for item in contents.split('\r\n'):
            if (not item.isspace()) and (not item == ""):
                if len(item) != 32:
                    print("Invalid hash in file:", item)
                else:
                    hashes.append(item)
        if md5str not in hashes:
            hashes.append(md5str)
        print(hashes)
    with open(allowedHash, "w") as file:
        file.write("\r\n".join(hashes))
except:
    print("Error opening server files, it may not exist.")

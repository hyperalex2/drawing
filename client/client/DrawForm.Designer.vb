﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DrawForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DrawForm))
        Me.txtGuess = New System.Windows.Forms.TextBox()
        Me.btnGuess = New System.Windows.Forms.Button()
        Me.panelConnect = New System.Windows.Forms.Panel()
        Me.btnMasterlistRefresh = New System.Windows.Forms.Button()
        Me.txtIpAddress = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblMasterList = New System.Windows.Forms.Label()
        Me.dgv_servers = New System.Windows.Forms.DataGridView()
        Me.servers_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.servers_Players = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.servers_Join = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.lblYourName = New System.Windows.Forms.Label()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.panelSettings = New System.Windows.Forms.Panel()
        Me.SettingsTabControl = New System.Windows.Forms.TabControl()
        Me.tab_UserList = New System.Windows.Forms.TabPage()
        Me.dgv_AdminPlace = New System.Windows.Forms.DataGridView()
        Me.user_Name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.user_FullName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.user_Score = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.user_Reset = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.user_Kick = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.user_Ban = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.user_Rank = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tab_UserSettings = New System.Windows.Forms.TabPage()
        Me.btnUserDecDiff = New System.Windows.Forms.Button()
        Me.btnUserIncDiff = New System.Windows.Forms.Button()
        Me.lblUserDifficulty = New System.Windows.Forms.Label()
        Me.AdminTab = New System.Windows.Forms.TabPage()
        Me.cb_WhoCanVote = New System.Windows.Forms.ComboBox()
        Me.cbAutoStart = New System.Windows.Forms.CheckBox()
        Me.btn_seeAchat = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_seebans = New System.Windows.Forms.Button()
        Me.cb_Difficulty = New System.Windows.Forms.ComboBox()
        Me.ManagerTab = New System.Windows.Forms.TabPage()
        Me.btn_forceend = New System.Windows.Forms.Button()
        Me.btn_forcestart = New System.Windows.Forms.Button()
        Me.AdminChatTab = New System.Windows.Forms.TabPage()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.txtSend = New System.Windows.Forms.TextBox()
        Me.rtb_Chat = New System.Windows.Forms.RichTextBox()
        Me.btnToMain = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.pCanvas = New System.Windows.Forms.Panel()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnBlack = New System.Windows.Forms.Button()
        Me.btnRed = New System.Windows.Forms.Button()
        Me.btnOrange = New System.Windows.Forms.Button()
        Me.btnYellow = New System.Windows.Forms.Button()
        Me.btnGreen = New System.Windows.Forms.Button()
        Me.btnBlue = New System.Windows.Forms.Button()
        Me.btnPurple = New System.Windows.Forms.Button()
        Me.btnPink = New System.Windows.Forms.Button()
        Me.pFrame = New System.Windows.Forms.Panel()
        Me.lblTimeLeft = New System.Windows.Forms.Label()
        Me.lblTimeLeftlbl = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.lblMisc = New System.Windows.Forms.Label()
        Me.timerTimeLeft = New System.Windows.Forms.Timer(Me.components)
        Me.btnBiggerBrush = New System.Windows.Forms.Button()
        Me.btnSmallerBrush = New System.Windows.Forms.Button()
        Me.btnPenSize = New System.Windows.Forms.Button()
        Me.timerChangeBrushSize = New System.Windows.Forms.Timer(Me.components)
        Me.btnWhite = New System.Windows.Forms.Button()
        Me.lbl = New System.Windows.Forms.Label()
        Me.lvLeaderboard = New System.Windows.Forms.ListView()
        Me.playername = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.score = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnPeach = New System.Windows.Forms.Button()
        Me.btnBrown = New System.Windows.Forms.Button()
        Me.btnVoteToStart = New System.Windows.Forms.Button()
        Me.lblMoreVotesNeededlbl = New System.Windows.Forms.Label()
        Me.lblMoreVotesNeeded = New System.Windows.Forms.Label()
        Me.btnGray = New System.Windows.Forms.Button()
        Me.panel_buttonColours = New System.Windows.Forms.Panel()
        Me.btnRectangleTool = New System.Windows.Forms.Button()
        Me.btnStraightLine = New System.Windows.Forms.Button()
        Me.btnLightGray = New System.Windows.Forms.Button()
        Me.btnYellowGreen = New System.Windows.Forms.Button()
        Me.btnLightSkyBlue = New System.Windows.Forms.Button()
        Me.timerFlashWhenMsg = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_EnterButton = New System.Windows.Forms.Button()
        Me.btnToSettings = New System.Windows.Forms.Button()
        Me.lvGuessHistory = New System.Windows.Forms.ListView()
        Me.panel_LobbyChat = New System.Windows.Forms.Panel()
        Me.rtb_lobby = New System.Windows.Forms.RichTextBox()
        Me.READONLYPANEL = New System.Windows.Forms.Panel()
        Me.timerDiscordUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.TimerToolChecker = New System.Windows.Forms.Timer(Me.components)
        Me.lblMasterListInfo = New System.Windows.Forms.Label()
        Me.panelConnect.SuspendLayout()
        CType(Me.dgv_servers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelSettings.SuspendLayout()
        Me.SettingsTabControl.SuspendLayout()
        Me.tab_UserList.SuspendLayout()
        CType(Me.dgv_AdminPlace, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab_UserSettings.SuspendLayout()
        Me.AdminTab.SuspendLayout()
        Me.ManagerTab.SuspendLayout()
        Me.AdminChatTab.SuspendLayout()
        Me.pFrame.SuspendLayout()
        Me.panel_buttonColours.SuspendLayout()
        Me.panel_LobbyChat.SuspendLayout()
        Me.READONLYPANEL.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtGuess
        '
        Me.txtGuess.BackColor = System.Drawing.SystemColors.HighlightText
        Me.txtGuess.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGuess.Location = New System.Drawing.Point(181, 602)
        Me.txtGuess.Name = "txtGuess"
        Me.txtGuess.Size = New System.Drawing.Size(337, 45)
        Me.txtGuess.TabIndex = 4
        '
        'btnGuess
        '
        Me.btnGuess.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btnGuess.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuess.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuess.ForeColor = System.Drawing.Color.White
        Me.btnGuess.Location = New System.Drawing.Point(524, 602)
        Me.btnGuess.Name = "btnGuess"
        Me.btnGuess.Size = New System.Drawing.Size(98, 45)
        Me.btnGuess.TabIndex = 5
        Me.btnGuess.Text = "guess"
        Me.btnGuess.UseVisualStyleBackColor = False
        '
        'panelConnect
        '
        Me.panelConnect.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.panelConnect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.panelConnect.Controls.Add(Me.lblMasterListInfo)
        Me.panelConnect.Controls.Add(Me.btnMasterlistRefresh)
        Me.panelConnect.Controls.Add(Me.txtIpAddress)
        Me.panelConnect.Controls.Add(Me.Label6)
        Me.panelConnect.Controls.Add(Me.lblMasterList)
        Me.panelConnect.Controls.Add(Me.dgv_servers)
        Me.panelConnect.Controls.Add(Me.txtName)
        Me.panelConnect.Controls.Add(Me.lblYourName)
        Me.panelConnect.Controls.Add(Me.btnConnect)
        Me.panelConnect.Location = New System.Drawing.Point(774, 8)
        Me.panelConnect.Name = "panelConnect"
        Me.panelConnect.Size = New System.Drawing.Size(736, 668)
        Me.panelConnect.TabIndex = 13
        '
        'btnMasterlistRefresh
        '
        Me.btnMasterlistRefresh.Location = New System.Drawing.Point(637, 24)
        Me.btnMasterlistRefresh.Name = "btnMasterlistRefresh"
        Me.btnMasterlistRefresh.Size = New System.Drawing.Size(75, 38)
        Me.btnMasterlistRefresh.TabIndex = 20
        Me.btnMasterlistRefresh.Text = "Refresh"
        Me.btnMasterlistRefresh.UseVisualStyleBackColor = True
        '
        'txtIpAddress
        '
        Me.txtIpAddress.Location = New System.Drawing.Point(546, 387)
        Me.txtIpAddress.Name = "txtIpAddress"
        Me.txtIpAddress.ReadOnly = True
        Me.txtIpAddress.Size = New System.Drawing.Size(169, 23)
        Me.txtIpAddress.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(389, 384)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(163, 26)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Will connect to: "
        '
        'lblMasterList
        '
        Me.lblMasterList.AutoSize = True
        Me.lblMasterList.BackColor = System.Drawing.Color.Transparent
        Me.lblMasterList.Font = New System.Drawing.Font("Trebuchet MS", 22.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMasterList.ForeColor = System.Drawing.Color.White
        Me.lblMasterList.Location = New System.Drawing.Point(3, 12)
        Me.lblMasterList.Name = "lblMasterList"
        Me.lblMasterList.Size = New System.Drawing.Size(326, 49)
        Me.lblMasterList.TabIndex = 17
        Me.lblMasterList.Text = "Server masterlist:"
        '
        'dgv_servers
        '
        Me.dgv_servers.AllowUserToAddRows = False
        Me.dgv_servers.AllowUserToDeleteRows = False
        Me.dgv_servers.AllowUserToResizeColumns = False
        Me.dgv_servers.AllowUserToResizeRows = False
        Me.dgv_servers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_servers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_servers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.servers_name, Me.servers_Players, Me.servers_Join})
        Me.dgv_servers.Location = New System.Drawing.Point(10, 64)
        Me.dgv_servers.Name = "dgv_servers"
        Me.dgv_servers.ReadOnly = True
        Me.dgv_servers.RowHeadersVisible = False
        Me.dgv_servers.RowTemplate.Height = 24
        Me.dgv_servers.Size = New System.Drawing.Size(703, 208)
        Me.dgv_servers.TabIndex = 16
        '
        'servers_name
        '
        Me.servers_name.HeaderText = "Name"
        Me.servers_name.Name = "servers_name"
        Me.servers_name.ReadOnly = True
        '
        'servers_Players
        '
        Me.servers_Players.HeaderText = "Players"
        Me.servers_Players.Name = "servers_Players"
        Me.servers_Players.ReadOnly = True
        '
        'servers_Join
        '
        Me.servers_Join.HeaderText = "Join"
        Me.servers_Join.Name = "servers_Join"
        Me.servers_Join.ReadOnly = True
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Trebuchet MS", 28.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(10, 420)
        Me.txtName.MaxLength = 10
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(705, 62)
        Me.txtName.TabIndex = 14
        '
        'lblYourName
        '
        Me.lblYourName.AutoSize = True
        Me.lblYourName.BackColor = System.Drawing.Color.Transparent
        Me.lblYourName.Font = New System.Drawing.Font("Trebuchet MS", 22.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYourName.ForeColor = System.Drawing.Color.White
        Me.lblYourName.Location = New System.Drawing.Point(3, 368)
        Me.lblYourName.Name = "lblYourName"
        Me.lblYourName.Size = New System.Drawing.Size(216, 49)
        Me.lblYourName.TabIndex = 4
        Me.lblYourName.Text = "your name:"
        '
        'btnConnect
        '
        Me.btnConnect.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnect.Font = New System.Drawing.Font("Trebuchet MS", 72.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConnect.ForeColor = System.Drawing.Color.White
        Me.btnConnect.Location = New System.Drawing.Point(10, 488)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(705, 149)
        Me.btnConnect.TabIndex = 15
        Me.btnConnect.Text = "connect"
        Me.btnConnect.UseVisualStyleBackColor = False
        '
        'panelSettings
        '
        Me.panelSettings.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.panelSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.panelSettings.Controls.Add(Me.SettingsTabControl)
        Me.panelSettings.Controls.Add(Me.btnToMain)
        Me.panelSettings.Controls.Add(Me.Label3)
        Me.panelSettings.Location = New System.Drawing.Point(800, 13)
        Me.panelSettings.Name = "panelSettings"
        Me.panelSettings.Size = New System.Drawing.Size(736, 668)
        Me.panelSettings.TabIndex = 14
        '
        'SettingsTabControl
        '
        Me.SettingsTabControl.Controls.Add(Me.tab_UserList)
        Me.SettingsTabControl.Controls.Add(Me.Tab_UserSettings)
        Me.SettingsTabControl.Controls.Add(Me.AdminTab)
        Me.SettingsTabControl.Controls.Add(Me.ManagerTab)
        Me.SettingsTabControl.Controls.Add(Me.AdminChatTab)
        Me.SettingsTabControl.Location = New System.Drawing.Point(3, 74)
        Me.SettingsTabControl.Name = "SettingsTabControl"
        Me.SettingsTabControl.SelectedIndex = 0
        Me.SettingsTabControl.Size = New System.Drawing.Size(708, 562)
        Me.SettingsTabControl.TabIndex = 55
        '
        'tab_UserList
        '
        Me.tab_UserList.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.tab_UserList.Controls.Add(Me.dgv_AdminPlace)
        Me.tab_UserList.Location = New System.Drawing.Point(4, 27)
        Me.tab_UserList.Name = "tab_UserList"
        Me.tab_UserList.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_UserList.Size = New System.Drawing.Size(700, 531)
        Me.tab_UserList.TabIndex = 0
        Me.tab_UserList.Text = "User List"
        '
        'dgv_AdminPlace
        '
        Me.dgv_AdminPlace.AllowUserToAddRows = False
        Me.dgv_AdminPlace.AllowUserToDeleteRows = False
        Me.dgv_AdminPlace.AllowUserToResizeColumns = False
        Me.dgv_AdminPlace.AllowUserToResizeRows = False
        Me.dgv_AdminPlace.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_AdminPlace.BackgroundColor = System.Drawing.Color.Linen
        Me.dgv_AdminPlace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_AdminPlace.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.user_Name, Me.user_FullName, Me.user_Score, Me.user_Reset, Me.user_Kick, Me.user_Ban, Me.user_Rank})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Trebuchet MS", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_AdminPlace.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgv_AdminPlace.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgv_AdminPlace.Location = New System.Drawing.Point(3, 3)
        Me.dgv_AdminPlace.Name = "dgv_AdminPlace"
        Me.dgv_AdminPlace.RowTemplate.Height = 24
        Me.dgv_AdminPlace.Size = New System.Drawing.Size(694, 320)
        Me.dgv_AdminPlace.TabIndex = 0
        '
        'user_Name
        '
        Me.user_Name.HeaderText = "Name"
        Me.user_Name.Name = "user_Name"
        Me.user_Name.ReadOnly = True
        '
        'user_FullName
        '
        Me.user_FullName.HeaderText = "Full Name"
        Me.user_FullName.Name = "user_FullName"
        Me.user_FullName.ReadOnly = True
        '
        'user_Score
        '
        Me.user_Score.HeaderText = "Score"
        Me.user_Score.Name = "user_Score"
        Me.user_Score.ReadOnly = True
        '
        'user_Reset
        '
        Me.user_Reset.HeaderText = "Reset"
        Me.user_Reset.Name = "user_Reset"
        Me.user_Reset.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.user_Reset.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'user_Kick
        '
        Me.user_Kick.HeaderText = "Kick"
        Me.user_Kick.Name = "user_Kick"
        '
        'user_Ban
        '
        Me.user_Ban.HeaderText = "Ban"
        Me.user_Ban.Name = "user_Ban"
        Me.user_Ban.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.user_Ban.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'user_Rank
        '
        Me.user_Rank.HeaderText = "Rank"
        Me.user_Rank.MaxInputLength = 1
        Me.user_Rank.Name = "user_Rank"
        Me.user_Rank.ReadOnly = True
        Me.user_Rank.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Tab_UserSettings
        '
        Me.Tab_UserSettings.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.Tab_UserSettings.Controls.Add(Me.btnUserDecDiff)
        Me.Tab_UserSettings.Controls.Add(Me.btnUserIncDiff)
        Me.Tab_UserSettings.Controls.Add(Me.lblUserDifficulty)
        Me.Tab_UserSettings.Location = New System.Drawing.Point(4, 25)
        Me.Tab_UserSettings.Name = "Tab_UserSettings"
        Me.Tab_UserSettings.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab_UserSettings.Size = New System.Drawing.Size(700, 533)
        Me.Tab_UserSettings.TabIndex = 4
        Me.Tab_UserSettings.Text = "User Settings"
        '
        'btnUserDecDiff
        '
        Me.btnUserDecDiff.Location = New System.Drawing.Point(162, 56)
        Me.btnUserDecDiff.Name = "btnUserDecDiff"
        Me.btnUserDecDiff.Size = New System.Drawing.Size(131, 25)
        Me.btnUserDecDiff.TabIndex = 2
        Me.btnUserDecDiff.Text = "Request Decrease"
        Me.btnUserDecDiff.UseVisualStyleBackColor = True
        '
        'btnUserIncDiff
        '
        Me.btnUserIncDiff.Location = New System.Drawing.Point(162, 25)
        Me.btnUserIncDiff.Name = "btnUserIncDiff"
        Me.btnUserIncDiff.Size = New System.Drawing.Size(131, 25)
        Me.btnUserIncDiff.TabIndex = 1
        Me.btnUserIncDiff.Text = "Request Increase"
        Me.btnUserIncDiff.UseVisualStyleBackColor = True
        '
        'lblUserDifficulty
        '
        Me.lblUserDifficulty.AutoSize = True
        Me.lblUserDifficulty.Location = New System.Drawing.Point(11, 46)
        Me.lblUserDifficulty.Name = "lblUserDifficulty"
        Me.lblUserDifficulty.Size = New System.Drawing.Size(123, 18)
        Me.lblUserDifficulty.TabIndex = 0
        Me.lblUserDifficulty.Text = "Current Difficulty:"
        '
        'AdminTab
        '
        Me.AdminTab.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.AdminTab.Controls.Add(Me.cb_WhoCanVote)
        Me.AdminTab.Controls.Add(Me.cbAutoStart)
        Me.AdminTab.Controls.Add(Me.btn_seeAchat)
        Me.AdminTab.Controls.Add(Me.Label5)
        Me.AdminTab.Controls.Add(Me.Label4)
        Me.AdminTab.Controls.Add(Me.btn_seebans)
        Me.AdminTab.Controls.Add(Me.cb_Difficulty)
        Me.AdminTab.Location = New System.Drawing.Point(4, 25)
        Me.AdminTab.Name = "AdminTab"
        Me.AdminTab.Padding = New System.Windows.Forms.Padding(3)
        Me.AdminTab.Size = New System.Drawing.Size(700, 533)
        Me.AdminTab.TabIndex = 1
        Me.AdminTab.Text = "Admin Settings"
        '
        'cb_WhoCanVote
        '
        Me.cb_WhoCanVote.FormattingEnabled = True
        Me.cb_WhoCanVote.Items.AddRange(New Object() {"Everyone", "Admins"})
        Me.cb_WhoCanVote.Location = New System.Drawing.Point(227, 11)
        Me.cb_WhoCanVote.Name = "cb_WhoCanVote"
        Me.cb_WhoCanVote.Size = New System.Drawing.Size(121, 26)
        Me.cb_WhoCanVote.TabIndex = 55
        '
        'cbAutoStart
        '
        Me.cbAutoStart.AutoSize = True
        Me.cbAutoStart.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAutoStart.Location = New System.Drawing.Point(11, 109)
        Me.cbAutoStart.Name = "cbAutoStart"
        Me.cbAutoStart.Size = New System.Drawing.Size(431, 30)
        Me.cbAutoStart.TabIndex = 54
        Me.cbAutoStart.Text = "Automatically start if at least two users on?"
        Me.cbAutoStart.UseVisualStyleBackColor = True
        '
        'btn_seeAchat
        '
        Me.btn_seeAchat.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btn_seeAchat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_seeAchat.Font = New System.Drawing.Font("Trebuchet MS", 7.0!)
        Me.btn_seeAchat.Location = New System.Drawing.Point(11, 142)
        Me.btn_seeAchat.Name = "btn_seeAchat"
        Me.btn_seeAchat.Size = New System.Drawing.Size(98, 57)
        Me.btn_seeAchat.TabIndex = 62
        Me.btn_seeAchat.Text = "Open Admin Chat"
        Me.btn_seeAchat.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 44)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(212, 26)
        Me.Label5.TabIndex = 56
        Me.Label5.Text = "Game-word difficulty:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(217, 26)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Who can vote to start:"
        '
        'btn_seebans
        '
        Me.btn_seebans.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btn_seebans.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_seebans.Font = New System.Drawing.Font("Trebuchet MS", 7.0!)
        Me.btn_seebans.Location = New System.Drawing.Point(117, 142)
        Me.btn_seebans.Name = "btn_seebans"
        Me.btn_seebans.Size = New System.Drawing.Size(98, 57)
        Me.btn_seebans.TabIndex = 60
        Me.btn_seebans.Text = "See Bans (0)"
        Me.btn_seebans.UseVisualStyleBackColor = False
        '
        'cb_Difficulty
        '
        Me.cb_Difficulty.FormattingEnabled = True
        Me.cb_Difficulty.Items.AddRange(New Object() {"Easy", "Medium", "Hard"})
        Me.cb_Difficulty.Location = New System.Drawing.Point(227, 47)
        Me.cb_Difficulty.Name = "cb_Difficulty"
        Me.cb_Difficulty.Size = New System.Drawing.Size(121, 26)
        Me.cb_Difficulty.TabIndex = 57
        '
        'ManagerTab
        '
        Me.ManagerTab.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.ManagerTab.Controls.Add(Me.btn_forceend)
        Me.ManagerTab.Controls.Add(Me.btn_forcestart)
        Me.ManagerTab.Location = New System.Drawing.Point(4, 25)
        Me.ManagerTab.Name = "ManagerTab"
        Me.ManagerTab.Padding = New System.Windows.Forms.Padding(3)
        Me.ManagerTab.Size = New System.Drawing.Size(700, 533)
        Me.ManagerTab.TabIndex = 2
        Me.ManagerTab.Text = "Manager Settings"
        '
        'btn_forceend
        '
        Me.btn_forceend.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btn_forceend.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_forceend.Location = New System.Drawing.Point(122, 11)
        Me.btn_forceend.Name = "btn_forceend"
        Me.btn_forceend.Size = New System.Drawing.Size(100, 62)
        Me.btn_forceend.TabIndex = 61
        Me.btn_forceend.Text = "Force END Game"
        Me.btn_forceend.UseVisualStyleBackColor = False
        '
        'btn_forcestart
        '
        Me.btn_forcestart.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btn_forcestart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_forcestart.Location = New System.Drawing.Point(16, 11)
        Me.btn_forcestart.Name = "btn_forcestart"
        Me.btn_forcestart.Size = New System.Drawing.Size(100, 62)
        Me.btn_forcestart.TabIndex = 49
        Me.btn_forcestart.Text = "Force Start Game"
        Me.btn_forcestart.UseVisualStyleBackColor = False
        '
        'AdminChatTab
        '
        Me.AdminChatTab.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.AdminChatTab.Controls.Add(Me.btnSend)
        Me.AdminChatTab.Controls.Add(Me.txtSend)
        Me.AdminChatTab.Controls.Add(Me.rtb_Chat)
        Me.AdminChatTab.Location = New System.Drawing.Point(4, 25)
        Me.AdminChatTab.Name = "AdminChatTab"
        Me.AdminChatTab.Padding = New System.Windows.Forms.Padding(3)
        Me.AdminChatTab.Size = New System.Drawing.Size(700, 533)
        Me.AdminChatTab.TabIndex = 3
        Me.AdminChatTab.Text = "Admin Chat"
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(589, 376)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(108, 46)
        Me.btnSend.TabIndex = 5
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'txtSend
        '
        Me.txtSend.AcceptsReturn = True
        Me.txtSend.Location = New System.Drawing.Point(3, 376)
        Me.txtSend.Multiline = True
        Me.txtSend.Name = "txtSend"
        Me.txtSend.Size = New System.Drawing.Size(569, 46)
        Me.txtSend.TabIndex = 4
        '
        'rtb_Chat
        '
        Me.rtb_Chat.Dock = System.Windows.Forms.DockStyle.Top
        Me.rtb_Chat.Location = New System.Drawing.Point(3, 3)
        Me.rtb_Chat.Name = "rtb_Chat"
        Me.rtb_Chat.Size = New System.Drawing.Size(694, 371)
        Me.rtb_Chat.TabIndex = 3
        Me.rtb_Chat.Text = ""
        '
        'btnToMain
        '
        Me.btnToMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btnToMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnToMain.Location = New System.Drawing.Point(611, 19)
        Me.btnToMain.Name = "btnToMain"
        Me.btnToMain.Size = New System.Drawing.Size(100, 53)
        Me.btnToMain.TabIndex = 48
        Me.btnToMain.Text = "Back"
        Me.btnToMain.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(359, 63)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Admin Settings"
        '
        'pCanvas
        '
        Me.pCanvas.BackColor = System.Drawing.Color.Linen
        Me.pCanvas.Location = New System.Drawing.Point(54, 50)
        Me.pCanvas.Name = "pCanvas"
        Me.pCanvas.Size = New System.Drawing.Size(331, 390)
        Me.pCanvas.TabIndex = 14
        '
        'btnClear
        '
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Location = New System.Drawing.Point(618, 107)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(100, 30)
        Me.btnClear.TabIndex = 15
        Me.btnClear.Text = "clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnBlack
        '
        Me.btnBlack.BackColor = System.Drawing.Color.Black
        Me.btnBlack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBlack.Location = New System.Drawing.Point(4, 10)
        Me.btnBlack.Name = "btnBlack"
        Me.btnBlack.Size = New System.Drawing.Size(56, 27)
        Me.btnBlack.TabIndex = 17
        Me.btnBlack.UseVisualStyleBackColor = False
        '
        'btnRed
        '
        Me.btnRed.BackColor = System.Drawing.Color.Red
        Me.btnRed.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRed.Location = New System.Drawing.Point(4, 472)
        Me.btnRed.Name = "btnRed"
        Me.btnRed.Size = New System.Drawing.Size(56, 27)
        Me.btnRed.TabIndex = 18
        Me.btnRed.UseVisualStyleBackColor = False
        '
        'btnOrange
        '
        Me.btnOrange.BackColor = System.Drawing.Color.Orange
        Me.btnOrange.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOrange.Location = New System.Drawing.Point(4, 406)
        Me.btnOrange.Name = "btnOrange"
        Me.btnOrange.Size = New System.Drawing.Size(56, 27)
        Me.btnOrange.TabIndex = 19
        Me.btnOrange.UseVisualStyleBackColor = False
        '
        'btnYellow
        '
        Me.btnYellow.BackColor = System.Drawing.Color.Yellow
        Me.btnYellow.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnYellow.Location = New System.Drawing.Point(4, 373)
        Me.btnYellow.Name = "btnYellow"
        Me.btnYellow.Size = New System.Drawing.Size(56, 27)
        Me.btnYellow.TabIndex = 20
        Me.btnYellow.UseVisualStyleBackColor = False
        '
        'btnGreen
        '
        Me.btnGreen.BackColor = System.Drawing.Color.Green
        Me.btnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGreen.Location = New System.Drawing.Point(4, 307)
        Me.btnGreen.Name = "btnGreen"
        Me.btnGreen.Size = New System.Drawing.Size(56, 27)
        Me.btnGreen.TabIndex = 21
        Me.btnGreen.UseVisualStyleBackColor = False
        '
        'btnBlue
        '
        Me.btnBlue.BackColor = System.Drawing.Color.Blue
        Me.btnBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBlue.Location = New System.Drawing.Point(4, 241)
        Me.btnBlue.Name = "btnBlue"
        Me.btnBlue.Size = New System.Drawing.Size(56, 27)
        Me.btnBlue.TabIndex = 22
        Me.btnBlue.UseVisualStyleBackColor = False
        '
        'btnPurple
        '
        Me.btnPurple.BackColor = System.Drawing.Color.Purple
        Me.btnPurple.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPurple.Location = New System.Drawing.Point(4, 208)
        Me.btnPurple.Name = "btnPurple"
        Me.btnPurple.Size = New System.Drawing.Size(56, 27)
        Me.btnPurple.TabIndex = 23
        Me.btnPurple.UseVisualStyleBackColor = False
        '
        'btnPink
        '
        Me.btnPink.BackColor = System.Drawing.Color.Pink
        Me.btnPink.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPink.Location = New System.Drawing.Point(4, 175)
        Me.btnPink.Name = "btnPink"
        Me.btnPink.Size = New System.Drawing.Size(56, 27)
        Me.btnPink.TabIndex = 24
        Me.btnPink.UseVisualStyleBackColor = False
        '
        'pFrame
        '
        Me.pFrame.BackColor = System.Drawing.Color.Transparent
        Me.pFrame.BackgroundImage = CType(resources.GetObject("pFrame.BackgroundImage"), System.Drawing.Image)
        Me.pFrame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pFrame.Controls.Add(Me.pCanvas)
        Me.pFrame.Location = New System.Drawing.Point(0, 0)
        Me.pFrame.Name = "pFrame"
        Me.pFrame.Size = New System.Drawing.Size(441, 473)
        Me.pFrame.TabIndex = 15
        '
        'lblTimeLeft
        '
        Me.lblTimeLeft.AutoSize = True
        Me.lblTimeLeft.Font = New System.Drawing.Font("Trebuchet MS", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimeLeft.Location = New System.Drawing.Point(541, 30)
        Me.lblTimeLeft.Name = "lblTimeLeft"
        Me.lblTimeLeft.Size = New System.Drawing.Size(95, 76)
        Me.lblTimeLeft.TabIndex = 25
        Me.lblTimeLeft.Text = "45"
        '
        'lblTimeLeftlbl
        '
        Me.lblTimeLeftlbl.AutoSize = True
        Me.lblTimeLeftlbl.Location = New System.Drawing.Point(559, 8)
        Me.lblTimeLeftlbl.Name = "lblTimeLeftlbl"
        Me.lblTimeLeftlbl.Size = New System.Drawing.Size(63, 18)
        Me.lblTimeLeftlbl.TabIndex = 26
        Me.lblTimeLeftlbl.Text = "time left"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(13, 58)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(353, 26)
        Me.lblStatus.TabIndex = 27
        Me.lblStatus.Text = "You are waiting for everyone to join. "
        '
        'lblMisc
        '
        Me.lblMisc.AutoSize = True
        Me.lblMisc.Font = New System.Drawing.Font("Trebuchet MS", 22.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMisc.Location = New System.Drawing.Point(9, 10)
        Me.lblMisc.Name = "lblMisc"
        Me.lblMisc.Size = New System.Drawing.Size(325, 49)
        Me.lblMisc.TabIndex = 28
        Me.lblMisc.Text = "Community draw!"
        '
        'timerTimeLeft
        '
        Me.timerTimeLeft.Interval = 1000
        '
        'btnBiggerBrush
        '
        Me.btnBiggerBrush.AccessibleDescription = "1"
        Me.btnBiggerBrush.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBiggerBrush.Location = New System.Drawing.Point(628, 525)
        Me.btnBiggerBrush.Name = "btnBiggerBrush"
        Me.btnBiggerBrush.Size = New System.Drawing.Size(100, 31)
        Me.btnBiggerBrush.TabIndex = 29
        Me.btnBiggerBrush.Text = "+"
        Me.btnBiggerBrush.UseVisualStyleBackColor = True
        '
        'btnSmallerBrush
        '
        Me.btnSmallerBrush.AccessibleDescription = "-1"
        Me.btnSmallerBrush.Font = New System.Drawing.Font("Trebuchet MS", 16.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSmallerBrush.Location = New System.Drawing.Point(628, 613)
        Me.btnSmallerBrush.Name = "btnSmallerBrush"
        Me.btnSmallerBrush.Size = New System.Drawing.Size(100, 34)
        Me.btnSmallerBrush.TabIndex = 30
        Me.btnSmallerBrush.Text = "-"
        Me.btnSmallerBrush.UseVisualStyleBackColor = True
        '
        'btnPenSize
        '
        Me.btnPenSize.BackColor = System.Drawing.Color.Black
        Me.btnPenSize.FlatAppearance.BorderSize = 0
        Me.btnPenSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPenSize.Location = New System.Drawing.Point(677, 586)
        Me.btnPenSize.Name = "btnPenSize"
        Me.btnPenSize.Size = New System.Drawing.Size(1, 1)
        Me.btnPenSize.TabIndex = 31
        Me.btnPenSize.UseVisualStyleBackColor = False
        '
        'timerChangeBrushSize
        '
        Me.timerChangeBrushSize.Interval = 30
        '
        'btnWhite
        '
        Me.btnWhite.BackColor = System.Drawing.Color.White
        Me.btnWhite.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnWhite.Location = New System.Drawing.Point(4, 109)
        Me.btnWhite.Name = "btnWhite"
        Me.btnWhite.Size = New System.Drawing.Size(56, 27)
        Me.btnWhite.TabIndex = 32
        Me.btnWhite.UseVisualStyleBackColor = False
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.Location = New System.Drawing.Point(13, 104)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(126, 26)
        Me.lbl.TabIndex = 33
        Me.lbl.Text = "Leaderboard"
        '
        'lvLeaderboard
        '
        Me.lvLeaderboard.BackColor = System.Drawing.Color.Linen
        Me.lvLeaderboard.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.playername, Me.score})
        Me.lvLeaderboard.Font = New System.Drawing.Font("Trebuchet MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvLeaderboard.FullRowSelect = True
        Me.lvLeaderboard.Location = New System.Drawing.Point(18, 133)
        Me.lvLeaderboard.Name = "lvLeaderboard"
        Me.lvLeaderboard.Size = New System.Drawing.Size(147, 147)
        Me.lvLeaderboard.TabIndex = 35
        Me.lvLeaderboard.UseCompatibleStateImageBehavior = False
        Me.lvLeaderboard.View = System.Windows.Forms.View.Details
        '
        'playername
        '
        Me.playername.Text = "Name"
        Me.playername.Width = 112
        '
        'score
        '
        Me.score.Text = "Score"
        Me.score.Width = 30
        '
        'btnPeach
        '
        Me.btnPeach.BackColor = System.Drawing.Color.Moccasin
        Me.btnPeach.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPeach.Location = New System.Drawing.Point(4, 142)
        Me.btnPeach.Name = "btnPeach"
        Me.btnPeach.Size = New System.Drawing.Size(56, 27)
        Me.btnPeach.TabIndex = 36
        Me.btnPeach.UseVisualStyleBackColor = False
        '
        'btnBrown
        '
        Me.btnBrown.BackColor = System.Drawing.Color.SaddleBrown
        Me.btnBrown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrown.Location = New System.Drawing.Point(4, 439)
        Me.btnBrown.Name = "btnBrown"
        Me.btnBrown.Size = New System.Drawing.Size(56, 27)
        Me.btnBrown.TabIndex = 37
        Me.btnBrown.UseVisualStyleBackColor = False
        '
        'btnVoteToStart
        '
        Me.btnVoteToStart.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btnVoteToStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoteToStart.Location = New System.Drawing.Point(372, 31)
        Me.btnVoteToStart.Name = "btnVoteToStart"
        Me.btnVoteToStart.Size = New System.Drawing.Size(138, 53)
        Me.btnVoteToStart.TabIndex = 38
        Me.btnVoteToStart.Text = "Vote to Start!"
        Me.btnVoteToStart.UseVisualStyleBackColor = False
        '
        'lblMoreVotesNeededlbl
        '
        Me.lblMoreVotesNeededlbl.AutoSize = True
        Me.lblMoreVotesNeededlbl.Location = New System.Drawing.Point(520, 86)
        Me.lblMoreVotesNeededlbl.Name = "lblMoreVotesNeededlbl"
        Me.lblMoreVotesNeededlbl.Size = New System.Drawing.Size(131, 18)
        Me.lblMoreVotesNeededlbl.TabIndex = 39
        Me.lblMoreVotesNeededlbl.Text = "more votes needed"
        '
        'lblMoreVotesNeeded
        '
        Me.lblMoreVotesNeeded.AutoSize = True
        Me.lblMoreVotesNeeded.Font = New System.Drawing.Font("Trebuchet MS", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoreVotesNeeded.Location = New System.Drawing.Point(549, 17)
        Me.lblMoreVotesNeeded.Name = "lblMoreVotesNeeded"
        Me.lblMoreVotesNeeded.Size = New System.Drawing.Size(64, 76)
        Me.lblMoreVotesNeeded.TabIndex = 40
        Me.lblMoreVotesNeeded.Text = "0"
        '
        'btnGray
        '
        Me.btnGray.BackColor = System.Drawing.Color.Gray
        Me.btnGray.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGray.Location = New System.Drawing.Point(4, 43)
        Me.btnGray.Name = "btnGray"
        Me.btnGray.Size = New System.Drawing.Size(56, 27)
        Me.btnGray.TabIndex = 41
        Me.btnGray.UseVisualStyleBackColor = False
        '
        'panel_buttonColours
        '
        Me.panel_buttonColours.AutoScroll = True
        Me.panel_buttonColours.AutoScrollMargin = New System.Drawing.Size(10, 10)
        Me.panel_buttonColours.Controls.Add(Me.btnRectangleTool)
        Me.panel_buttonColours.Controls.Add(Me.btnStraightLine)
        Me.panel_buttonColours.Controls.Add(Me.btnLightGray)
        Me.panel_buttonColours.Controls.Add(Me.btnBrown)
        Me.panel_buttonColours.Controls.Add(Me.btnBlack)
        Me.panel_buttonColours.Controls.Add(Me.btnYellowGreen)
        Me.panel_buttonColours.Controls.Add(Me.btnWhite)
        Me.panel_buttonColours.Controls.Add(Me.btnRed)
        Me.panel_buttonColours.Controls.Add(Me.btnPeach)
        Me.panel_buttonColours.Controls.Add(Me.btnLightSkyBlue)
        Me.panel_buttonColours.Controls.Add(Me.btnPink)
        Me.panel_buttonColours.Controls.Add(Me.btnOrange)
        Me.panel_buttonColours.Controls.Add(Me.btnPurple)
        Me.panel_buttonColours.Controls.Add(Me.btnBlue)
        Me.panel_buttonColours.Controls.Add(Me.btnYellow)
        Me.panel_buttonColours.Controls.Add(Me.btnGreen)
        Me.panel_buttonColours.Controls.Add(Me.btnGray)
        Me.panel_buttonColours.Location = New System.Drawing.Point(618, 144)
        Me.panel_buttonColours.Name = "panel_buttonColours"
        Me.panel_buttonColours.Size = New System.Drawing.Size(150, 375)
        Me.panel_buttonColours.TabIndex = 42
        '
        'btnRectangleTool
        '
        Me.btnRectangleTool.BackColor = System.Drawing.Color.White
        Me.btnRectangleTool.BackgroundImage = Global.client.My.Resources.Resources.RectangleTool
        Me.btnRectangleTool.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnRectangleTool.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRectangleTool.Location = New System.Drawing.Point(66, 43)
        Me.btnRectangleTool.Name = "btnRectangleTool"
        Me.btnRectangleTool.Size = New System.Drawing.Size(56, 27)
        Me.btnRectangleTool.TabIndex = 46
        Me.btnRectangleTool.UseVisualStyleBackColor = False
        '
        'btnStraightLine
        '
        Me.btnStraightLine.BackColor = System.Drawing.Color.White
        Me.btnStraightLine.BackgroundImage = CType(resources.GetObject("btnStraightLine.BackgroundImage"), System.Drawing.Image)
        Me.btnStraightLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnStraightLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStraightLine.Location = New System.Drawing.Point(66, 10)
        Me.btnStraightLine.Name = "btnStraightLine"
        Me.btnStraightLine.Size = New System.Drawing.Size(56, 27)
        Me.btnStraightLine.TabIndex = 45
        Me.btnStraightLine.UseVisualStyleBackColor = False
        '
        'btnLightGray
        '
        Me.btnLightGray.BackColor = System.Drawing.Color.LightGray
        Me.btnLightGray.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLightGray.Location = New System.Drawing.Point(4, 76)
        Me.btnLightGray.Name = "btnLightGray"
        Me.btnLightGray.Size = New System.Drawing.Size(56, 27)
        Me.btnLightGray.TabIndex = 44
        Me.btnLightGray.UseVisualStyleBackColor = False
        '
        'btnYellowGreen
        '
        Me.btnYellowGreen.BackColor = System.Drawing.Color.YellowGreen
        Me.btnYellowGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnYellowGreen.Location = New System.Drawing.Point(4, 340)
        Me.btnYellowGreen.Name = "btnYellowGreen"
        Me.btnYellowGreen.Size = New System.Drawing.Size(56, 27)
        Me.btnYellowGreen.TabIndex = 42
        Me.btnYellowGreen.UseVisualStyleBackColor = False
        '
        'btnLightSkyBlue
        '
        Me.btnLightSkyBlue.BackColor = System.Drawing.Color.LightSkyBlue
        Me.btnLightSkyBlue.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLightSkyBlue.Location = New System.Drawing.Point(4, 274)
        Me.btnLightSkyBlue.Name = "btnLightSkyBlue"
        Me.btnLightSkyBlue.Size = New System.Drawing.Size(56, 27)
        Me.btnLightSkyBlue.TabIndex = 43
        Me.btnLightSkyBlue.UseVisualStyleBackColor = False
        '
        'timerFlashWhenMsg
        '
        Me.timerFlashWhenMsg.Enabled = True
        Me.timerFlashWhenMsg.Interval = 200
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 304)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 26)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Previous guesses:"
        '
        'btn_EnterButton
        '
        Me.btn_EnterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btn_EnterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_EnterButton.Location = New System.Drawing.Point(0, 0)
        Me.btn_EnterButton.Name = "btn_EnterButton"
        Me.btn_EnterButton.Size = New System.Drawing.Size(1, 1)
        Me.btn_EnterButton.TabIndex = 46
        Me.btn_EnterButton.Text = "Vote to Start!"
        Me.btn_EnterButton.UseVisualStyleBackColor = False
        '
        'btnToSettings
        '
        Me.btnToSettings.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(204, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.btnToSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnToSettings.Location = New System.Drawing.Point(628, 31)
        Me.btnToSettings.Name = "btnToSettings"
        Me.btnToSettings.Size = New System.Drawing.Size(100, 53)
        Me.btnToSettings.TabIndex = 47
        Me.btnToSettings.Text = "Settings"
        Me.btnToSettings.UseVisualStyleBackColor = False
        '
        'lvGuessHistory
        '
        Me.lvGuessHistory.Location = New System.Drawing.Point(18, 335)
        Me.lvGuessHistory.Name = "lvGuessHistory"
        Me.lvGuessHistory.Size = New System.Drawing.Size(147, 311)
        Me.lvGuessHistory.TabIndex = 48
        Me.lvGuessHistory.UseCompatibleStateImageBehavior = False
        Me.lvGuessHistory.View = System.Windows.Forms.View.List
        '
        'panel_LobbyChat
        '
        Me.panel_LobbyChat.Controls.Add(Me.rtb_lobby)
        Me.panel_LobbyChat.Font = New System.Drawing.Font("Trebuchet MS", 8.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel_LobbyChat.Location = New System.Drawing.Point(0, 0)
        Me.panel_LobbyChat.Name = "panel_LobbyChat"
        Me.panel_LobbyChat.Size = New System.Drawing.Size(441, 473)
        Me.panel_LobbyChat.TabIndex = 0
        '
        'rtb_lobby
        '
        Me.rtb_lobby.Location = New System.Drawing.Point(14, 29)
        Me.rtb_lobby.Name = "rtb_lobby"
        Me.rtb_lobby.Size = New System.Drawing.Size(441, 473)
        Me.rtb_lobby.TabIndex = 0
        Me.rtb_lobby.Text = ""
        '
        'READONLYPANEL
        '
        Me.READONLYPANEL.Controls.Add(Me.pFrame)
        Me.READONLYPANEL.Controls.Add(Me.panel_LobbyChat)
        Me.READONLYPANEL.Location = New System.Drawing.Point(171, 114)
        Me.READONLYPANEL.Name = "READONLYPANEL"
        Me.READONLYPANEL.Size = New System.Drawing.Size(441, 473)
        Me.READONLYPANEL.TabIndex = 49
        '
        'timerDiscordUpdate
        '
        Me.timerDiscordUpdate.Interval = 2000
        '
        'TimerToolChecker
        '
        Me.TimerToolChecker.Enabled = True
        Me.TimerToolChecker.Interval = 250
        '
        'lblMasterListInfo
        '
        Me.lblMasterListInfo.AutoSize = True
        Me.lblMasterListInfo.BackColor = System.Drawing.Color.Transparent
        Me.lblMasterListInfo.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMasterListInfo.ForeColor = System.Drawing.Color.White
        Me.lblMasterListInfo.Location = New System.Drawing.Point(21, 275)
        Me.lblMasterListInfo.Name = "lblMasterListInfo"
        Me.lblMasterListInfo.Size = New System.Drawing.Size(628, 26)
        Me.lblMasterListInfo.TabIndex = 21
        Me.lblMasterListInfo.Text = "There are [] players connected in [] servers, with a total score of []."
        '
        'DrawForm
        '
        Me.AcceptButton = Me.btn_EnterButton
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(145, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(173, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1521, 688)
        Me.Controls.Add(Me.panelConnect)
        Me.Controls.Add(Me.panelSettings)
        Me.Controls.Add(Me.READONLYPANEL)
        Me.Controls.Add(Me.btnToSettings)
        Me.Controls.Add(Me.btn_EnterButton)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.panel_buttonColours)
        Me.Controls.Add(Me.lblMoreVotesNeeded)
        Me.Controls.Add(Me.lblMoreVotesNeededlbl)
        Me.Controls.Add(Me.btnVoteToStart)
        Me.Controls.Add(Me.lvLeaderboard)
        Me.Controls.Add(Me.lbl)
        Me.Controls.Add(Me.btnPenSize)
        Me.Controls.Add(Me.btnSmallerBrush)
        Me.Controls.Add(Me.btnBiggerBrush)
        Me.Controls.Add(Me.lblMisc)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.lblTimeLeftlbl)
        Me.Controls.Add(Me.btnGuess)
        Me.Controls.Add(Me.txtGuess)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.lblTimeLeft)
        Me.Controls.Add(Me.lvGuessHistory)
        Me.Font = New System.Drawing.Font("Trebuchet MS", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DrawForm"
        Me.Padding = New System.Windows.Forms.Padding(10)
        Me.Text = "Draw!"
        Me.panelConnect.ResumeLayout(False)
        Me.panelConnect.PerformLayout()
        CType(Me.dgv_servers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelSettings.ResumeLayout(False)
        Me.panelSettings.PerformLayout()
        Me.SettingsTabControl.ResumeLayout(False)
        Me.tab_UserList.ResumeLayout(False)
        CType(Me.dgv_AdminPlace, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab_UserSettings.ResumeLayout(False)
        Me.Tab_UserSettings.PerformLayout()
        Me.AdminTab.ResumeLayout(False)
        Me.AdminTab.PerformLayout()
        Me.ManagerTab.ResumeLayout(False)
        Me.AdminChatTab.ResumeLayout(False)
        Me.AdminChatTab.PerformLayout()
        Me.pFrame.ResumeLayout(False)
        Me.panel_buttonColours.ResumeLayout(False)
        Me.panel_LobbyChat.ResumeLayout(False)
        Me.READONLYPANEL.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtGuess As TextBox
    Friend WithEvents btnGuess As Button
    Friend WithEvents panelConnect As Panel
    Friend WithEvents btnConnect As Button
    Friend WithEvents pCanvas As Panel
    Friend WithEvents txtName As TextBox
    Friend WithEvents lblYourName As Label
    Friend WithEvents btnClear As Button
    Friend WithEvents btnBlack As Button
    Friend WithEvents btnRed As Button
    Friend WithEvents btnOrange As Button
    Friend WithEvents btnYellow As Button
    Friend WithEvents btnGreen As Button
    Friend WithEvents btnBlue As Button
    Friend WithEvents btnPurple As Button
    Friend WithEvents btnPink As Button
    Friend WithEvents pFrame As Panel
    Friend WithEvents lblTimeLeft As Label
    Friend WithEvents lblTimeLeftlbl As Label
    Friend WithEvents lblStatus As Label
    Friend WithEvents lblMisc As Label
    Friend WithEvents timerTimeLeft As Timer
    Friend WithEvents btnBiggerBrush As Button
    Friend WithEvents btnSmallerBrush As Button
    Friend WithEvents btnPenSize As Button
    Friend WithEvents timerChangeBrushSize As Timer
    Friend WithEvents btnWhite As Button
    Friend WithEvents lbl As Label
    Friend WithEvents lvLeaderboard As ListView
    Friend WithEvents playername As ColumnHeader
    Friend WithEvents score As ColumnHeader
    Friend WithEvents btnPeach As Button
    Friend WithEvents btnBrown As Button
    Friend WithEvents btnVoteToStart As Button
    Friend WithEvents lblMoreVotesNeededlbl As Label
    Friend WithEvents lblMoreVotesNeeded As Label
    Friend WithEvents btnGray As Button
    Friend WithEvents btnLightGray As Button
    Friend WithEvents btnYellowGreen As Button
    Friend WithEvents btnLightSkyBlue As Button
    Friend WithEvents timerFlashWhenMsg As Timer
    Friend WithEvents panel_buttonColours As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents btn_EnterButton As Button
    Friend WithEvents btnToSettings As Button
    Friend WithEvents panelSettings As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents btnToMain As Button
    Friend WithEvents dgv_AdminPlace As DataGridView
    Friend WithEvents lvGuessHistory As ListView
    Friend WithEvents cbAutoStart As CheckBox
    Friend WithEvents cb_WhoCanVote As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cb_Difficulty As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents btn_forcestart As Button
    Friend WithEvents btn_seebans As Button
    Friend WithEvents btn_forceend As Button
    Friend WithEvents btn_seeAchat As Button
    Friend WithEvents SettingsTabControl As TabControl
    Friend WithEvents tab_UserList As TabPage
    Friend WithEvents AdminTab As TabPage
    Friend WithEvents ManagerTab As TabPage
    Friend WithEvents AdminChatTab As TabPage
    Friend WithEvents btnSend As Button
    Friend WithEvents txtSend As TextBox
    Public WithEvents rtb_Chat As RichTextBox
    Friend WithEvents panel_LobbyChat As Panel
    Friend WithEvents rtb_lobby As RichTextBox
    Friend WithEvents READONLYPANEL As Panel
    Friend WithEvents timerDiscordUpdate As Timer
    Friend WithEvents user_Name As DataGridViewTextBoxColumn
    Friend WithEvents user_FullName As DataGridViewTextBoxColumn
    Friend WithEvents user_Score As DataGridViewTextBoxColumn
    Friend WithEvents user_Reset As DataGridViewButtonColumn
    Friend WithEvents user_Kick As DataGridViewButtonColumn
    Friend WithEvents user_Ban As DataGridViewButtonColumn
    Friend WithEvents user_Rank As DataGridViewTextBoxColumn
    Friend WithEvents Tab_UserSettings As TabPage
    Friend WithEvents btnUserDecDiff As Button
    Friend WithEvents btnUserIncDiff As Button
    Friend WithEvents lblUserDifficulty As Label
    Friend WithEvents btnStraightLine As Button
    Friend WithEvents TimerToolChecker As Timer
    Friend WithEvents btnRectangleTool As Button
    Friend WithEvents txtIpAddress As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents lblMasterList As Label
    Friend WithEvents dgv_servers As DataGridView
    Friend WithEvents servers_name As DataGridViewTextBoxColumn
    Friend WithEvents servers_Players As DataGridViewTextBoxColumn
    Friend WithEvents servers_Join As DataGridViewButtonColumn
    Friend WithEvents btnMasterlistRefresh As Button
    Friend WithEvents lblMasterListInfo As Label
End Class

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("client")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Hewlett-Packard")>
<Assembly: AssemblyProduct("client")>
<Assembly: AssemblyCopyright("Copyright © Hewlett-Packard 2017")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("050cd4ba-35f4-47c8-a424-7af283422813")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

<Assembly: AssemblyVersion("4.2.4.4")>
<Assembly: AssemblyFileVersion("4.2.4.4")>

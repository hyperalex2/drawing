﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace DiscordDLL
{
    public class DiscordApp
    {
        static DiscordApp()
        {
            if(!System.IO.File.Exists(DLLName))
            {
                Console.WriteLine("ERROR: '" + DLLName + "' is not present, unable to use discord functionality!");
                throw new System.IO.FileNotFoundException("DLL required is not present for Discord functionality.", DLLName, null);
            }
        }

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ReadyCallback();

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void DisconnectedCallback(int errorCode, string message);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ErrorCallback(int errorCode, string message);

        public struct EventHandlers
        {
            public ReadyCallback readyCallback;
            public DisconnectedCallback disconnectedCallback;
            public ErrorCallback errorCallback;
        }

        // Values explanation and example: https://discordapp.com/developers/docs/rich-presence/how-to#updating-presence-update-presence-payload-fields
        [System.Serializable]
        public struct RichPresence
        {
            public string state; /* max 128 bytes */
            public string details; /* max 128 bytes */
            public long startTimestamp;
            public long endTimestamp;
            public string largeImageKey; /* max 32 bytes */
            public string largeImageText; /* max 128 bytes */
            public string smallImageKey; /* max 32 bytes */
            public string smallImageText; /* max 128 bytes */
            public string partyId; /* max 128 bytes */
            public int partySize;
            public int partyMax;
            public string matchSecret; /* max 128 bytes */
            public string joinSecret; /* max 128 bytes */
            public string spectateSecret; /* max 128 bytes */
            public bool instance;
        }
        const string DLLName = "DrawingEngine.dll";
        [DllImport(DLLName, EntryPoint = "Discord_Initialize", CallingConvention = CallingConvention.Cdecl)]
        private static extern void Initialize(string applicationId, ref EventHandlers handlers, bool autoRegister, string optionalSteamId);

        [DllImport(DLLName, EntryPoint = "Discord_UpdatePresence", CallingConvention = CallingConvention.Cdecl)]
        private static extern void UpdatePresence(ref RichPresence presence);

        [DllImport(DLLName, EntryPoint = "Discord_RunCallbacks", CallingConvention = CallingConvention.Cdecl)]
        public static extern void RunCallbacks();

        [DllImport(DLLName, EntryPoint = "Discord_Shutdown", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Shutdown();

        public static void UpdatePres(string state, string details, long startTime, long endTime)
        {
            RichPresence newPres = new RichPresence();
            newPres.details = state;
            newPres.state = details;
            newPres.startTimestamp = startTime;
            newPres.endTimestamp = endTime;
            UpdatePresence(ref newPres);
        }

        public static void Init(string applicationID)
        {
            EventHandlers newHandle = new EventHandlers();
            Initialize(applicationID, ref newHandle, true, "noidea");
        }
    }
}
